---
title: J'aime
lang: fr
---

[See also the English version](../likes/)

## Vidéos

### Chaînes

- [Calmos](https://www.youtube.com/@Calmos/videos) : cinéma
- [Monsieur Phi](https://www.youtube.com/@MonsieurPhi) : philosophie
- [Un Créatif](https://www.youtube.com/@UnCreatif) : marketing
- [Stupid Economics](https://www.youtube.com/@StupidEco) : économie

### Vidéos

- [Infernet (playlist)](https://www.youtube.com/playlist?list=PLv1KZC6gJTFnOvSLchy-SHfp5UD-zrISt)


## Films

- [Adolescentes (2019)](https://www.imdb.com/title/tt10785618/)
- [Kiss & Cry (2017)](https://www.imdb.com/title/tt6796306/) (voir [la bande-annonce sur YouTube](https://www.youtube.com/watch?v=puyj3tRGAqA))
- [L'ennemi de la classe (2013)](https://www.imdb.com/title/tt3187076/) (langue: slovène et allemand)
- [Les garçons et Guillaume, à table! (2013)](https://www.imdb.com/title/tt2315200/)
- [Willy 1er (2016)](https://www.imdb.com/title/tt5698748/) (voir [la bande-annonce sur Youtube](https://www.youtube.com/watch?v=8IgOg9alMN8))
- [Les Beaux Gosses (2009)](https://www.imdb.com/title/tt1314237)


## Livres

- [« Confiteor » de Jaume Cabré (2013)](https://www.babelio.com/livres/Cabre-Confiteor/497369)
- [« Le monde est clos et le désir infini » de Daniel Cohen (2015)](https://www.babelio.com/livres/Cohen-Le-monde-est-clos-et-le-desir-infini/766768)

J'aime particulièrement les biographies:

- [« Marquis de Sade : L'ange de l'ombre » de Gonzague Saint Bris (2013)](https://www.babelio.com/livres/Saint-Bris-Marquis-de-Sade--Lange-de-lombre/562241)
- [« François 1er et la Renaissance » de Gonzague Saint Bris (2010)](https://www.babelio.com/livres/Saint-Bris-Francois-1er-et-la-Renaissance/183867)
- [« La Fayette » de Gonzague Saint Bris (2007)](https://www.babelio.com/livres/Saint-Bris-La-Fayette/10674)
- [« Henri IV et la France réconciliée » de Gonzague Saint Bris (2012)](https://www.babelio.com/livres/Saint-Bris-Henri-IV-et-la-France-reconciliee/172050)
- [« Louis XIV et le grand siècle » de Gonzague Saint Bris (2012)](https://www.babelio.com/livres/Saint-Bris-Louis-XIV-et-le-grand-siecle/424132)
- [« Belle Greene » de Alexandra Lapierre (2021)](https://www.babelio.com/livres/Lapierre-Belle-Greene/1278254)
- [« Léonard de Vinci » de Sophie Chauveau (2008)](https://www.babelio.com/livres/Chauveau-Leonard-de-Vinci/131388)


## Podcasts

J'écoute principalement des émissions de France Culture:

- [L'Esprit Public](https://www.radiofrance.fr/franceculture/podcasts/l-esprit-public): débats sur d'actualité
- [Entendez-Vous L'Éco](https://www.radiofrance.fr/franceculture/podcasts/entendez-vous-l-eco): économie
- [Le Cours de l'histoire](https://www.radiofrance.fr/franceculture/podcasts/le-cours-de-l-histoire): histoire
- [La Revue de presse internationale](https://www.radiofrance.fr/franceculture/podcasts/revue-de-presse-internationale)
- [Le Temps du débat](https://www.radiofrance.fr/franceculture/podcasts/le-temps-du-debat): débats sur l'actualité
- [Les Pieds sur terre](https://www.radiofrance.fr/franceculture/podcasts/les-pieds-sur-terre): reportages sans commentaires
- [LSD, la série documentaire](https://www.radiofrance.fr/franceculture/podcasts/lsd-la-serie-documentaire)
- [Carbone 14, le magazine de l'archéologie](https://www.radiofrance.fr/franceculture/podcasts/carbone-14-le-magazine-de-l-archeologie)
