---
title: Sur les radiations émises par phosphorescence
lang: fr
date: 1896-02-24
params:
    author: Henri Becquerel
    original_pub_info: Comptes rendus hebdomadaires des séances de l'Académie des sciences, séance du 24 février 1896
    has_katex: true
---

[voir sur gallica.bnf.fr](https://gallica.bnf.fr/ark:/12148/bpt6k30780/f422.item)

Dans une précédente séance, M. Ch. Henry a annoncé que le sulfure de zinc phosphorescent interposé sur le trajet de rayons émanés d’un tube de Crookes augmentait l’intensité des radiations traversant l’aluminium.

D’autre part, M. Niewenglowski a reconnu que le sulfure de calcium phosphorescent du commerce émet des radiations qui traversent les corps opaques.

Ce fait s’étend à divers corps phosphorescents et, en particulier, aux sels d’urane dont la phosphorescence a une très courte durée.

Avec le sulfate double d’uranium et de potassium, dont je possède des cristaux formant une croûte mince et transparente, j’ai pu faire l’expérience suivante :

On enveloppe une plaque photographique Lumière, au gélatinobromure, avec deux feuilles de papier noir très épais, tel que la plaqué ne se voile pas par une exposition au Soleil, durant une journée.

On pose sur la feuille de papier, à l’extérieur, une plaque de la substance phosphorescente, et l’on expose le tout au Soleil, pendant plusieurs heures. Lorsqu’on développe ensuite la plaque photographique, on reconnaît que la silhouette de la substance phosphorescente apparaît en noir sur le cliché. Si l’on interpose entre la substance phosphorescente et le papier une pièce de monnaie, ou un écran métallique percé d’un dessin à jour, on voit l’image de ces objets apparaître sur le cliché.

On peut répéter les mêmes expériences en interposant entre la substance phosphorescente et le papier une mince lame de verre, ce qui exclut la possibilité d’une action chimique due à des vapeurs qui pourraient émaner de la substance échauffée par les rayons solaires.

On doit donc conclure de ces expériences que la substance phosphorescente en question émet des radiations qui traversent le papier opaque à la lumière et réduisent les sels d’argent.

