---
title: Sur les radiations invisibles émisés par les sels d’uranium
lang: fr
date: 1896-03-23
params:
    author: Henri Becquerel
    original_pub_info: Comptes rendus hebdomadaires des séances de l'Académie des sciences, séance du 23 mars 1896
    has_katex: true
---

[voir sur gallica.bnf.fr](https://gallica.bnf.fr/ark:/12148/bpt6k30780/f691.item)

## Action sur les corps électrisés.

Dans une des dernières séances de l’Académie, j’ai annoncé que les radiations invisibles émises par les sels d’uranium avaient la propriété de décharger les corps électrisés : j’ai continué l’étude de ce phénomène au moyen de l’électroscope de M. Hurmuzescu et j’ai pu constater, autrement que je l’avais fait par la photographie, que les radiations en question traversent divers corps opaques, en particulier l’aluminium et le cuivre. Le platine a présenté une absorption beaucoup plus considérable que les deux métaux précédents.

Lorsque l’on suit le rapprochement progressif des feuilles d’or de l’électroscope pendant la décharge, on reconnaît que, pour des écarts qui ne dépassent pas 3o°, les variations angulaires sont très sensiblement proportionnelles au temps, de sorte que la vitesse du rapprochement ou la fraction de degré dont les feuilles d’or se rapprochent en une seconde peut donner une idée des intensités relatives des radiations actives. Je rapporterai seulement ici les nombres relatifs à l’absorption au travers d’une lame de quartz, perpendiculaire à l’axe et ayant 5 mm d’épaisseur. Les vitesses sont exprimées en secondes d’arc et en secondes de temps.

Une lamelle de sulfate double d’uranyle et de potassium placée au-dessous des feuilles d’or, dissipait la charge de l’électroscope avec une vitesse représentée par 22,50. L’interposition de la lame de quartz a réduit la vitesse à 5,43 ; le rapport des deux nombres est 4,15.

J’ai cherché si les radiations émanées de la paroi phosphorescente d’un tube de Crookes étaient affaiblies par la même lame de quartz dans un rapport qui fût du même ordre de grandeur. Un tube de Crookes a été disposé à l’extérieur de l’électroscope, en regard d’une des faces de la lanterne dont le verre avait été remplacé par une plaque d’aluminium de
0,12 mm d’épaisseur, et devant cette plaque on avait placé un écran en cuivre percé d’un trou circulaire de 15 mm de diamètre. Les radiations au travers du cuivre sont assez affaiblies pour que leur effet pût être négligé dans l’expérience présente. Lorsque le tube de Crookes était excité par une bobine d’induction, les feuilles d’or de l’électroscope se rapprochaient rapidement, environ de 1° en 1,4 s, ce qui correspond à une vitesse de 2671,4 exprimée au moyen des unités adoptées plus haut.

Lorsque la lame de quartz bouchait l’ouverture circulaire, la vitesse de la chute des feuilles d’or devenait 163,63, soit 15,7 fois plus petite.

L’affaiblissement est près de quatre fois plus grand dans le second cas que dans le premier, mais il est du même ordre de grandeur. C’est le seul point que cette expérience mette en évidence. L’observation n’est
pas contraire à l’hypothèse probable qui attribuerait la différence à ce que les rayons émis par le sel d’urane et les rayons émis par le tube ou par le
verre phosphorescent n’ont pas les mêmes longueurs d’onde, mais les conditions différentes des deux expériences ne permettent pas d’affirmer cette hétérogénéité.

L’électroscope a permis de mettre également en évidence la faible différence entre l’émission d’une lamelle de sel d’urane maintenue depuis onze jours à l’obscurité, et l’émission de la même lamelle vivement éclairée au magnésium. Dans le premier cas, la vitesse de la chute des femlles était 20,69, et après l’excitation lumineuse elle est devenue 23,08.

On ignore ce que deviennent les charges électriques ainsi dissipées, comme si les diélectriques étaient rendus conducteurs pendant qu’ils sont traversés par ces radiations. L’expérience a montré que la lamelle cristalline convenablement isolée ne se chargeait pas, tout en déchargeant l’électromètre ; en outre, une lamelle mise pendant longtemps en présence de l’appareil ne lui a communiqué aucune charge.


## Emission par divers sels d’uranium. Persistance. Excitation.

Si le phénomène de l’émission des radiations invisibles que nous étudions est un phénomène de phosphorescence, on doit pouvoir mettre en évidence l’excitation par des radiations déterminées. Cette étude est rendue très difficile par la persistance prodigieuse de l’émission lorsque les corps sont maintenus à l’obscurité, à l’abri des radiations lumineuses ou des radiations invisibles dont nous connaissons la nature. Au bout de plus de quinze jours, les sels d’urane émettent encore des radiations presque aussi intenses que le premier jour. En disposant sur une même plaque photographique, au travers du papier noir, une lamelle maintenue longtemps à l’obscurité et une autre qui vient d’être exposée à la lumière du jour, l’impression de la silhouette de la seconde est un peu plus forte que la première. La lumière du magnésium n’a produit dans les mêmes conditions qu’un effet inappréciable. Si l’on éclaire vivement les lamelles de sulfate double d’uranyle et de potassium, avec l’arc électrique, ou avec les étincelles brillantes de la décharge d’une bouteille de Leyde, les impressions sont notablement plus noires. Le phénomène paraît donc bien être un phénomène de phosphorescence invisible, mais qui ne semble pas intimement lié à la phosphorescence ou à la fluorescence visible. En effet, si les sels de sesquioxyde d’urane sont très fluorescents, on sait que les sels verts uraneux, dont j’ai eu occasion d’étudier les curieuses propriétés absorbantes, ne sont ni phosphorescents, ni fluorescents. Or le sulfate uraneux se comporte comme le sulfate uranique, et émet des radiations invisibles aussi intenses.

Je rapporterai encore une autre expérience intéressante. On sait que le nitrate d’urane cesse d’être phosphorescent ou fluorescent lorsqu’il est en dissolution, on fondu dans son eau de cristallisation. J’ai pris alors un cristal de ce sel et, après l’avoir disposé dans un petit tube fermé par une plaque mince de verre, je l’ai chauffé à l’obscurité de manière à éviter même les radiations de la lampe à alcool qui l’échauffait ; le sel a fondu, puis je l’ai laissé cristalliser à l’obscurité et je l’ai placé ensuite sur une plaque photographique, enduite de papier noir, en préservant toujours le sel de l’action de la lumière. On pouvait s’attendre à n’observer aucun effet, toute excitation lumineuse ayant été évitée depuis le moment où le corps avait cessé d’être phosphorescent, et cependant l’impression fut aussi forte que pour les sels exposés à la lumière, et même, aux points où le sel adhérait à la plaque de verre, l’impression a été plus forte que celle d’un échantillon de sulfate uranique mis en expérience comparative sur la même plaque.

Sur cette même plaque photographique se trouvaient encore des cristaux de nitrate d’urane, reposant sur les lamelles de verre par des faces différentes, et pour lesquels les effets ont été sensiblement les mêmes.

J’ai disposé aussi des surfaces unies, formées de sulfate uranique et de sulfate double uranico-potassique, et j’ai projeté sur ces surfaces le spectre de l’arc électrique, au travers d’un appareil en quartz. Les bandes d’excitation ultra-violette se sont alors dessinées très nettement par fluorescence, mais, lorsque j’ai reproduit la silhouette de ces surfaces sur une plaque photographique, la silhouette est devenue presque uniformément noire, indiquant, soit que l’émission propre de la substance masque les faibles différences que l’on pouvait observer pour les différentes régions d’excitation, soit que l’excitation n’a pas lieu dans la région du spectre qui se projetait sur la surface étudiée.


## Absorption par diverses substances.

On peut très facilement étudier qualitativement l’absorption des radiations qui nous occupent au travers de diverses substances en disposant sur une même plaque photographique des lames de ces substances ou des petits tubes plats pleins de liquides et en les couvrant par une lamelle de sulfate double uranico-potassique, ou par tout autre sel d’urane.

Avec diverses substances mises sous des épaisseurs différant peu de 2 mm, j’ai reconnu que l’eau était très transparente ; la plupart des dissolutions, même les dissolutions de sels métalliques, des dissolutions de nitrate de cuivre, de chlorure d’or, de nitrate d’urane, une solution alcoolique de chlorophylle se sont comportées comme assez transparentes ; il en a été de même de la paraffine, de la cire à modeler ; le verre d’urane a été plus opaque, de même un verre coloré en rouge ; l’aluminium sous cette épaisseur est peu transparent, l’étain est plus opaque, et un verre bleu au cobalt s’est montré plus opaque que les métaux précédents.

Dans une autre série d’expériences j’avais disposé des cristaux divers, et diverses combinaisons optiques destinées à manifester les phénomènes de double réfraction et de polarisation. Les images obtenues ont été trop faibles pour que je donne aujourd’hui les résultats ; cependant, on reconnaît que le quartz absorbe plus ces radiations invisibles que le spath d’Islande ; le soufre natif s’est comporté comme transparent.

Enfin, les expériences dans l’air et dans l’air raréfié dont j’ai parlé à la fin de ma dernière Note, tout en ne donnant pas des différences très notables, montrent nettement que les épreuves dans l’air raréfié sont un peu plus fortes, ce qui manifesterait une absorption par l’air.


## Réfraction.

Les faits que j’ai signalés dans ma dernière Note ont mis en évidence la réfraction au travers du verre. À ces expériences on peut ajouter la suivante : Sur l’une des faces d’un prisme de crown, à quelques millimètres de l’arête, on fixe, parallèlement à celle-ci, un petit tube de verre très mince, de 1 mm environ de diamètre, rempli de nitrate d’urane cristallisé et formant une source linéaire d’émission de radiations invisibles.

On applique alors l’autre face du prisme sur la plaque photographique. En développant la plaque trois jours après on a reconnu une impression diffuse sous la base du prisme, impression séparée de la trace de l’arête par une ligne blanche, et dont le déplacement est de l’ordre de grandeur de celui qu’on obtient dans les mêmes conditions pour la lumière. La diminution considérable de l’intensité lumineuse lorsque les sources s’éloignent un peu de la plaque photographique n’a pas permis jusqu’ici de faire des mesures d’indices de réfraction.


## Anomalies présentées par diverses substances.

Les sels d’urane émettent des radiations invisibles avec une constance remarquable, mais il n’en est pas de même d’autres substances phosphorescentes.

J’avais obtenu, avec du sulfure de calcium, des résultats de l’ordre de ceux que donnent les sels d’urane, et j’ai signalé, dans ma dernière Note, une épreuve d’une remarquable intensité au travers de 2 mm d’aluminium. La même matière phosphorescente, placée sur une seconde plaque photographique, dans les mêmes conditions, s’est montrée inactive, et, depuis, je n’ai réussi à obtenir aucune image avec des sulfures de calcium ; j’avais eu le même insuccès avec des échantillons de blende hexagonale de diverses provenances. J’ai alors cherché à communiquer une activité nouvelle à ces corps par les divers procédés connus. Je les ai échauffés en présence de la plaque photographique sans échauffer celle-ci, et je n’ai pu obtenir aucune impression. Dans une autre série d’expériences, les diverses substances ont été refroidies à -20°, excitées par la lumière du jour et du magnésium, puis placées sur la plaque photographique ; seuls les sels d’urane ont donné des images.

Enfin, j’ai excité les sulfures et la blende hexagonale par les étincelles de la décharge d’une batterie, et les substances, rendues vivement phosphorescentes, n’ont encore manifesté aucune activité au travers du papier noir. J’ai appris, au cours de ces expériences, que notre éminent Confrère M. Troost, avait observé un fait analogue. Les échantillons très ancienne de blende hexagonale, qui lui avaient d’abord donné des résultats énergiques, ont donné ensuite des résultats progressivement décroissants, puis sont devenus inactifs. Il y a là un fait très curieux dont les experiences ultérieures nous donneront peut-être l’explication.
