---
title: Sur la loi de la décharge dans l’air de l'uranium électrisé
lang: fr
date: 1897-04-12
params:
    author: Henri Becquerel
    original_pub_info: Comptes rendus hebdomadaires des séances de l'Académie des sciences, séance du 12 avril 1897
    has_katex: true
---

[voir sur gallica.bnf.fr]()

Dans les recherches dont j’ai communiqué à l’Académie les principaux résultats, j’ai montré que, par un effet du rayonnement qui lui est propre, l’uranium métallique ne conserve pas une charge électrique qui lui est communiquée.

La décharge se produit par l’air ou par le gaz ambiant, qui paraît être un intermédiaire nécessaire. Lorsqu’on dispose dans le vide une boule d’uranium métallique convenablement isolée et chargée d’électricité, la déperdition spontanée s’annule ou du moins devient de l’ordre de grandeur de la déperdition par les supports.

Je communiquerai ultérieurement les résultats obtenus dans divers gaz et à diverses pressions. J’indiquerai seulement dans la présente Note la loi de déperdition de l’électricité par l’uranium, en fonction du temps, et du potentiel des corps électrisés.

Dans une précédente Communication, j’ai déjà indiqué que, pour les faibles potentiels, la fonction qui lie le potentiel et le temps était analogue à celle qui exprime le refroidissement des corps, c’est-à-dire que la vitesse de chute du potentiel, $- \frac{dV}{dt}$ était sensiblement proportionnelle au potentiel V; au contraire pour les potentiels élevés la valeur de $\frac{dV}{dt}$ augmente très lentement avec le potentiel et tend vers une constante. Les expériences présentes montrent comment se concilient ces deux lois limites.

Les nombres qui suivent sont relatifs à la variation spontanée, avec le temps, du potentiel d’une sphère d’uranium métallique, de 13,7 mm de diamètre, isolée, et en relation avec l’aiguille d’un électromètre à quadrants isolé par de la paraffine.

La quantité d’électricité $\frac{dQ}{dt} - C \frac{dV}{dt}$ qu’un système électrisé perd en une seconde est à la fois fonction de la capacité C et de la vitesse de chute du potentiel ; aussi, pour que les variations du potentiel mesurent le débit d’électricité, est-il nécessaire que la capacité du système reste constante.

Cette condition est remplie par l’aiguille de l’électromèlre, lorsque le déplacement angulaire de celle-ci est proportionnel à son potentiel, et lorsque les potentiels des quadrants restent constants ; il est facile de le démontrer. Mais si d’une série à une autre on fait varier la charge des quadrants pour faire varier la sensibilité de l’instrument, la capacité de l’aiguille change, et les résultats obtenus dans les mesures ne sont plus directement comparables.

Ces diverses conditions et, en particulier, la difficulté, soit de maintenir suffisamment constantes les faibles capacités des systèmes étudiés, soit de mesurer ces capacités, ont rendu les mesures assez délicates. On a représenté graphiquement les résultats obtenus, et on a tracé des courbes moyennes qui éliminent en partie les erreurs accidentelles dues aux imperfections de l’électromètre.

L’isolement électrique n’étant pas complet, on a étudié séparément la déperdition par les supports, et la vitesse de déperdition par cette cause a été retranchée de la vitesse observée pour avoir l’effet dû à l’uranium.

Les piles qui ont servi à graduer l’instrument étaient des piles à zinc, cuivre, eau et glycérine, montées depuis plusieurs années ; chaque élément valait, en moyenne, 0,93 volts. Les nombres suivants sont donnés en prenant ces éléments de pile pour unité de potentiel.

Avec une première disposition, les quadrants étant chargés par une pile de 64 éléments dont le milieu était à la Terre, on a eu les nombres suivants qui résument de nombreuses mesures.

<table>
    <thead>
        <tr>
            <th scope="column" rowspan=2>
                Potentiels.
            </th>
            <th colspan=2>
                Chute de potentiel en une seconde
            </th>
        </tr>
        <tr>
            <th scope="column">observée.</th>
            <th scope="column">corrigée.</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>32,25</td>
            <td>0,1940</td>
            <td>0,187</td>
        </tr>
        <tr>
            <td>32,15</td>
            <td>0,1884</td>
            <td>0,182</td>
        </tr>
        <tr>
            <td>29,0</td>
            <td>0,1843</td>
            <td>0,179</td>
        </tr>
        <tr>
            <td>27,4</td>
            <td>0,1770</td>
            <td>0,175</td>
        </tr>
        <tr>
            <td>21,2</td>
            <td>0,1670</td>
            <td>0,164</td>
        </tr>
        <tr>
            <td>17,8</td>
            <td>0,1615</td>
            <td>0,159</td>
        </tr>
        <tr>
            <td>14,5</td>
            <td>0,1535</td>
            <td>0,152</td>
        </tr>
        <tr>
            <td>11,32</td>
            <td>0,1384</td>
            <td>0,137</td>
        </tr>
        <tr>
            <td>8,12</td>
            <td>0,1191</td>
            <td>0,118</td>
        </tr>
        <tr>
            <td>5,70</td>
            <td>0,1018</td>
            <td>0,101</td>
        </tr>
        <tr>
            <td>4,05</td>
            <td>0,0880</td>
            <td>0,088</td>
        </tr>
        <tr>
            <td>2,43</td>
            <td>0,0609</td>
            <td>0,061</td>
        </tr>
        <tr>
            <td>1,20</td>
            <td>0,0340</td>
            <td>0,034</td>
        </tr>
    </tbody>
</table>

Une seconde série a été faite dans des conditions un peu différentes en chargeant les quadrants avec une pile de 8 éléments, dont le milieu était à la Terre. La sensibilité étant moindre, on a eu des déviations de l’aiguille dans les mêmes limites que les précédentes, mais pour des potentiels plus élevés. Les résultats obtenus sont résumés dans les nombres suivants

<table>
    <thead>
        <tr>
            <th scope="column" rowspan=2>
                Potentiels.
            </th>
            <th colspan=2>
                Chute de potentiel en une seconde
            </th>
        </tr>
        <tr>
            <th scope="column">observée.</th>
            <th scope="column">corrigée.</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>227</td>
            <td>0,2778</td>
            <td>0,233</td>
        </tr>
        <tr>
            <td>206,5</td>
            <td>0,2744</td>
            <td>0,233</td>
        </tr>
        <tr>
            <td>175,5</td>
            <td>0,2673</td>
            <td>0,232</td>
        </tr>
        <tr>
            <td>145</td>
            <td>0,2592</td>
            <td>0,230</td>
        </tr>
        <tr>
            <td>114</td>
            <td>0,2470</td>
            <td>0,224</td>
        </tr>
        <tr>
            <td>93,8</td>
            <td>0,2376</td>
            <td>0,219</td>
        </tr>
        <tr>
            <td>72,6</td>
            <td>0,2253</td>
            <td>0,211</td>
        </tr>
        <tr>
            <td>62,3</td>
            <td>0,2188</td>
            <td>0,206</td>
        </tr>
        <tr>
            <td>52</td>
            <td>0,2103</td>
            <td>0,199</td>
        </tr>
        <tr>
            <td>36,5</td>
            <td>0,1929</td>
            <td>0,185</td>
        </tr>
        <tr>
            <td>26,2</td>
            <td>0,1754</td>
            <td>0,170</td>
        </tr>
    </tbody>
</table>

Ces nombres sont très sensiblement représentés dans les deux séries par l’expression

$$
\frac{dV}{dt} \left(a + \frac{b}{V}\right) = -1
$$

On a trouvé, pour $a$ et $b$, les valeurs $a = 4,53$ et $b = 31$ dans la première série, et $a = 4,06$, $b = 47$ dans la seconde.

Une autre série, qui n’est pas rapportée ici, avait donné $a = 4,6$ et $b = 20$. Les divergences entre ces nombres sont dues en partie aux erreurs d’expérience ; le coefficient $a$ est mieux déterminé que $b$.

Il est facile de voir que les coefficients $a$ et $b$ doivent être proportionnels à la capacité du système ; en effet, le débit $-C \frac{dV}{dt}$ d’électricité par la surface de la sphère doit être fonction seulement de la différence du potentiel entre cette sphère et le milieu ambiant ; on doit donc avoir, pour des systèmes de capacités $C$ et $C^\prime$,

$$
C \left( \frac{dV}{dt} \right) = C^\prime \left( \frac{dV}{dt} \right)^\prime
$$

d’où l’on déduit immédiatement

$$
\frac{a}{C} = \frac{a^\prime}{C^\prime} = \alpha
\quad\quad \text{et} \quad\quad
\frac{b}{C} = \frac{b^\prime}{C^\prime} = \beta
$$

$\alpha$ et $\beta$ étant des valeurs qui dépendent des surfaces d’émission et du milieu ambiant.

L’expérience prouve, du reste, cette proportionnalité.

En adjoignant à l’aiguille de l’électromètre une sphère de 8,11 cm de rayon, la capacité a été augmentée dans le rapport de 0,33 à 1, et la valeur $\frac{dV}{dt}$  a été réduite à 0,328 de sa valeur.

L’expression précédente montre que, si le potentiel V est petit, le terme $\frac{b}{V}$ est grand par rapport à $a$, et que l’on tend vers l’expression

$$
\frac{1}{V} \frac{dV}{dt} = - \frac{1}{b^\prime}
$$

tandis que si V est très grand, le terme $\frac{b}{V}$ devient très petit et l’on a sensiblement

$$
\frac{dV}{dt} = - \frac{1}{a^\prime}
$$

ainsi que le prouve l’expérience.

On voit aussi que les grandeurs absolues des potentiels pour lesquelles le terme $\frac{b}{V}$ ou $\frac{\beta C}{V}$ est grand ou petit par rapport au coefficient $\alpha C$, varient comme la capacité du système.

À l’occasion de cette Note, je signalerai à l’Académie que les sels d’urane que je conserve depuis plus d’un an, à l’abri de tout rayonnement connu, continuent à émettre avec une intensité à peine décroissante des radiations qui donnent des impressions photographiques au travers des corps opaques.

