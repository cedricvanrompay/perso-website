---
title: Sur les propriétés différentes des radiations invisibles émises par les sels d’uranium, et du rayonnement de la paroi anticathodique d’un tube de Crookes
lang: fr
date: 1896-03-30
params:
    author: Henri Becquerel
    original_pub_info: Comptes rendus hebdomadaires des séances de l'Académie des sciences, séance du 30 mars 1896
    has_katex: true
---

[voir sur gallica.bnf.fr](https://gallica.bnf.fr/ark:/12148/bpt6k30780/f764.item)

L’étude des propriétés des radiations émises par les sels d’uranium, dont j’ai déjà entretenu l’Académie dans les précédentes séances, et auxquelles s’ajoutent quelques propriétés nouvelles qui sont décrites ci-après, permet d’établir des différences importantes entre les effets de ces radiations et les effets produits par le rayonnement de la paroi anticathodique d’un tube de Crookes, tels que les a décrits et appliqués M. Rontgen.


## Double réfraction. Polarisation et dichroïsme au travers d’une tourmaline.

Dans le but de rechercher si les radiations émises par les sels d’uranium se polarisent, j’ai disposé l’expérience suivante : une lame mince de tourmaline, parallèle à l’axe et de 0,50 mm d’épaisseur, a été coupée en deux ; les deux moitiés ont été juxtaposées de façon que leurs axes fussent rectangulaires, et cet ensemble a été recouvert par une lame unique, parallèle à l’axe, de 0,88 mm d’épaisseur, et dont l’axe était parallèle à l’axe de l’une des deux moitiés de la première tourmaline. Dans ces conditions la lumière ordinaire est transmise au travers des deux tourmalines dont les axes sont parallèles, et est arrêtée au travers de l’autre moitié du système. Cet ensemble, ainsi réglé, a été déposé sur une plaque photographique préalablement enveloppée de papier noir, et a été couvert par une lamelle de sulfate double d’uranyle et de potassium. La plaque photographique, développée au bout de soixante heures, a très nettement montré la silhouette des tourmalines, et l’action au travers des tourmalines parallèles était notablement plus forte qu’au travers des tourmalines croisées. Or au travers des deux moitiés de ce système les substances traversées sont les mêmes. La différence observée ne peut être attribuée qu’au dichroïsme.

Cette expérience montre donc à la fois, pour les rayons invisibles émis par les sels d’uranium, la double réfraction, la polarisation des deux rayons et leur inégale absorption au travers de la tourmaline.

La même expérience, répétée avec les mêmes tourmalines et le rayonnement émané d’un tube de Crookes, a donné un résultat négatif ; les deux plages ont également absorbé le rayonnement. Ce résultat, qui avait déjà été signalé par M. Röntgen est d’accord avec l’absence de réfraction appréciable. Il ne prouve pas que le rayonnement ne se polarise pas, mais seulement que l’absorption est la même pour les tourmalines parallèles et pour les tourmalines croisées.


## Absorption inégale par diverses substances.

L’absorption des deux rayonnements qui nous occupent, lorsqu’ils traversent les mêmes substances, présente des caractères très différents. On peut le constater, soit par la méthode photographique qui donne des résultats qualitatifs, soit par l'action sur l’électroscope qui fournit des mesures relatives.


### Méthode photographique.

Lorsqu’on projette le rayonnement d’un tube de Crookes sur une plaque photographique enveloppée de papier noir, et couverte par divers corps en lames minces ou par de petits tubes plats pleins de divers liquides, on observe d’abord que, pour une courte pose, la plupart des corps, sauf la paraffine qui est très transparente, et l’aluminium qui l’est un peu moins, se comportent comme ayant des opacités assez voisines. On reconnaît cependant que l’eau est beaucoup plus opaque que la paraffine, une solution de nitrate d’urane s’est montrée plus opaque que des solutions de nitrate de cuivre et de chlorure d’or.

Si l’on pose davantage on reconnaît qu’une lame de cuivre de 0,04 mm est traversée, mais sous l’épaisseur de 0,95 mm le cuivre est très peu transparent. Le platine, sous l’épaisseur de 0,08 mm, paraît un des plus opaques parmi les corps étudiés. Sur les mêmes plaques se trouvaient encore du zinc, du plomb, de l’argent, du verre, du spath, du quartz, du sel gemme, etc. Le verre, sous l’épaisseur de 2,13 mm, le quartz perpendicufaire à l’axe (2,05 mm), le spath (1,93 mm et 2,40 mm), le sel gemme, ont paru aussi peu transparents que le cuivre.

L’absorption des rayons émis par un sel d’urane est très différente. L’aluminium et la paraffine sont toujours très transparents, mais les métaux se laissent bien plus facilement traverser que par le rayonnement d’un tube de Crookes ; le cuivre (0,10 mm) est très transparent, le platine (0,08 mm) également, mais un peu moins que le cuivre ; l’argent laisse aussi passer ces radiations ; ainsi que le zinc, le plomb (0,36 mm) s’est comporté comme opaque.

Le quartz (4,66 mm), le spath d’Islande (4,48 mm) sont très peu transparents ; le soufre (2,01 mm) l’est davantage.

On voit déjà, par cet aperçu, que les radiations émises par les sels d’uranium traversent plus facilement la plupart des corps, et en particulier les métaux, que ne le fait le rayonnement d’un tube de Crookes.


### Méthode électroscopique

La décharge d’un corps électrisé par les radiations ayant traversé divers écrans conduit à la même conclusion. J’ai déjà montré que le quartz absorbe moins les radiations des sels d’uranium que le rayonnement d’un tube de Crookes.

Lorsqu’on fait agir un tube de Crookes sur les feuilles d’or d’un électroscope, une lame d’aluminium, de 0,10 mm d’épaisseur, laisse passer un rayonnement intense, et la chute des feuilles d’or se fait en quelques secondes ; si l’on interpose alors une lame de cuivre, de 0,10 mm d’épaisseur, les feuilles d’or cessent de se rapprocher, ou du moins ne se rapprochent qu’avec une extrême lenteur. Le platine intercepte encore davantage le rayonnement.

Il n’en est pas de même avec les rayons émis par les sels d’uranium ; ceux-ci traversent beaucoup plus facilement le cuivre et le platine. Je rapporterai ici quelques nombres mesurant l’action au travers de ces deux métaux. L’expérience était disposée de la manière suivante : une lamelle de sulfate double d’uranyle et de potassium était placée à 2 cm environ au-dessous des feuilles d’or de l’électroscope. On a étudié alors la déperdition de celui-ci, lorsque la lamelle agissait seule, ou lorsqu’on interposait soit successivement, soit simultanément, des écrans formés de lames d’aluminium, de cuivre ou de platine. La lame d’aluminium avait 0,10 mm d’épaisseur, la lame de cuivre 0,035 mm et la lame de platine 0,035 mm. Les actions sont mesurées par la vitesse de la chute des feuilles d’or, ou par la fraction de degré dont leur angle diminue en une seconde ; on sait que cette diminution est sensiblement proportionnelle au temps. Les nombres suivants
expriment les vitesses en secondes d’angle et en secondes de temps.

<table>
    <caption>
        Action d’une lame de sulfate double
        d’uranyle et de potassium
        sur les feuilles d’or d’un électroscope.
    </caption>
    <thead>
        <tr>
            <th scope="col">Nature des écrans.</th>
            <th scope="col">
                Date et heure moyenne des mesures.
            </th>
            <th scope="col">Vitesse de la chute.</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Sans écran</td>
            <td>28 mars à 1 h 45 min</td>
            <td>38,18</td>
        </tr>
        <tr>
            <td>Écran d'aluminium (0,10 mm)</td>
            <td>28 mars à 3 h 00 min</td>
            <td>9,42</td>
        </tr>
        <tr>
            <td>Écran de cuivre (0,09 mm)</td>
            <td>28 mars à 3 h 50 min</td>
            <td>11,40</td>
        </tr>
        <tr>
            <td>Écran de platine (0,035 mm)</td>
            <td>28 mars à 5 h 00 min</td>
            <td>9,60</td>
        </tr>
        <tr>
            <td>Platine et aluminium superposés</td>
            <td>28 mars à 5 h 50 min</td>
            <td>6,53</td>
        </tr>
        <tr>
            <td>Sans écran</td>
            <td>28 mars à 6 h 20 min</td>
            <td>33,60</td>
        </tr>
        <tr>
            <td>Aluminium et cuivre superposés</td>
            <td>28 mars à 6 h 40</td>
            <td>7,44</td>
        </tr>
        <tr>
            <td>Sans écran (le lendemain)</td>
            <td>29 mars à 5 h 40</td>
            <td>33,00</td>
        </tr>
    </tbody>
</table>

On reconnaît que le cuivre et l’aluminium ont à peu près la même absorption pour la même épaisseur, que le platine absorbe un peu davantage, et que l’absorption des écrans superposés est moindre que la somme des effets dus à chacun d’eux, comme dans les expériences de thermochrose de Melloni, et comme cela a été constaté par le rayonnement anticathodique par MM. Hurmuzescu et Benofst.

Les radiations émises par la lamelle de sel d’urane ne sont donc pas homogènes.

Dans une expérience que j’avais faite la semaine dernière j’avais observé que l’électroscope se déchargeait au travers d’un écran de cuivre de 1,40 mm d’épaisseur.

Les nombres ci-dessus montrent encore que, peu de temps après avoir été exposée à la lumière, l’action de la lamelle de sel d’uranium était un peu plus forte. Il s’est produit, en cinq heures, un léger affaiblissement puis l’action est restée sensiblement constante jusqu’au lendemain.


## Sur quelques propriétés particulières de l’émission des radiations par les sels d’uranium.

J’ai déjà signalé l’indépendance entre l’émission des radiations invisibles des sels d’uranium et l’émission des radiations visibles, par phosphorescence ; en particulier les sels uraneux, qui ne sont pas phosphorescents, émettent des radiations invisibles. J’ai indiqué aussi que le nitrate d’urane fondu et ayant cristallisé à l’obscurité était aussi actif que les cristaux du même sel exposé à la lumière ; j’ai vérifié récemment que le nitrate d’urane en solution dans l’eau est encore aussi actif, alors que cette dissolution n’est plus fluorescente. C’est un nouvel exemple d’indépendance entre les deux phénomènes d’émission.

J’ai cherché également si ces radiations communiqueraient une phosphorescence invisible aux sulfures qui sont devenus inactifs ou aux divers échantillons de blende hexagonale que je possède. Le résultat a été négatif, du moins comme effet immédiat. De même, l’action du rayonnement d’un tube de Crookes n’a communiqué aucune activité à la blende hexagonale, soit pendant l’excitation, soit après, en laissant la pose se prolonger pendant trois jours.

Je n’ai pas observé de différence appréciable entre l’activité d’une lamelle de sulfate double d’uranyle et de potassium, exposée au rayonnement d’un tube de Crookes, et une lamelle non exposée. Pendant l’influence directe de ce rayonnement sur une plaque photographique, la lamelle s’est comportée comme opaque. Elle a été placée ensuite sur une autre plaque photographique à côté d’une lamelle du même sel, et les deux lamelles ont donné des impressions identiques.

Je dois citer encore une expérience qui paraît en contradiction avec les phénomènes de réflexion et de réfraction que j’ai observés.

Entre deux lamelles de verre de même épaisseur (1,83 mm dans une expérience et 1,37 mm dans une autre), j’ai tassé de la poudre de verre obtenue en pulvérisant un morceau du même verre, et la poudre, tassée légèrement, affleurait la surface des lames de verre. Dans ces conditions, la bande de verre pulvérisé apparaît comme opaque à la lumière ordinaire. On sait que le rayonnement d’un tube de Crookes la traverse avec la même facilité qu’une lame de verre homogène : c’est une des expériences fondamentales de M. Rontgen. Dans les conditions qui viennent d’être indiquées et avec les radiations émises par les sels d’urane, la bande de verre pulvérisé s’est comportée comme notablement plus transparente que les lames de verre voisines. Comme la quantité de matière traversée est sensiblement moindre, on ne peut déduire aucune conclusion certaine de cette expérience contradictoire.


## Considérations générales.

Il serait prématuré de tirer des conclusions absolues des expériences qui précèdent. Si l’on n’avait égard qu’aux seuls phénomènes d’absorption on rendrait compte des faits en admettant que les radiations émises par les sels d’uranium et le rayonnement d’un tube de Crookes se comportent comme ayant des longueurs d’onde différentes, mais l’absence de réflexion et de réfraction, bien constatée pour le rayonnement étudié par M. Rontgen, établit une différence plus profonde. Il semble plus probable de penser que la phosphorescence de la tache anticathodique n’est qu’un phénomène concomitant d’un phénomène électrique, dont celle-ci serait le siège, et que c’est ce phénomène électrique, une sorte d’effluve, comme cela résulte des expériences de M. H. Dufour, qui provoque la phosphorescence de la plaque photographique et, par suite, la réduction des sels d’argent par les radiations phosphorescentes excitées sur place. Quant à la phosphorescence du verre des tubes de Crookes, il est possible qu’elle soit accompagnée de radiations analogues à celles qu’émettent les sels d’uranium, mais il est probable qu’une très longue pose serait nécessaire pour les mettre en évidence.