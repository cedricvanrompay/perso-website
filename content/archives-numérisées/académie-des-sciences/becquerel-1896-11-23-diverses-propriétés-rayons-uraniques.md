---
title: Sur diverses propriétés des rayons uraniques
lang: fr
date: 1896-11-23
params:
    author: Henri Becquerel
    original_pub_info: Comptes rendus hebdomadaires des séances de l'Académie des sciences, séance du 23 novembre 1896
    has_katex: true
---

[voir sur gallica.bnf.fr](https://gallica.bnf.fr/ark:/12148/bpt6k30799/f855.item)

J’ai montré, il y a plusieurs mois, que l’uranium et ses sels émettent des radiations invisibles qui traversent les corps opaques et déchargent à distance les corps électrisés. Ces radiations présentent des propriétés communes avec le phénomène appelé _rayons X_ par M. Röntgen, mais en diffèrent parce qu’elles se réfléchissent et se réfractent comme la lumière. Parmi les propriétés que j’ai observées en poursuivant l’étude de l’émission de ces radiations que, pour abréger, j’appellerai _radiations uraniques_, il en est deux qu’il me semble intéressant de signaler aujourd’hui : ce sont la durée de l’émission, et la faculté de communiquer à des gaz la
propriété de décharger les corps électrisés.


## Durée de l’émission.

J’avais reconnu déjà que, au bout dé plusieurs semaines, des sels d’uranium, maintenus à l’obscurité soit dans une boîte en carton, soit dans une boîte en plomb, continuent à émettre des radiations. Divers sels d’uranium, phosphorescents et non phosphorescents, uraniques et uraneux, dont quelques-uns étaient déjà maintenus à l’obscurité depuis le 3 mars, ont été enfermés le 3 mai dernier dans une double boîte en plomb épais, qui n’a pas quitté un réduit obscur où la lumière du jour ne pénètre pas. Ces sels ont été tous fixés sur une lamelle de verre, et quelques-uns enfermés dans une petite cloche en verre scellée à la lamelle inférieure par de la paraffine, de façon à exclure toute possibilité d’action par des vapeurs. Les lamelles de verre reposent sur une feuille de papier noir, tendue à 1 cm environ au-dessus du fond de la boîte intérieure, et une disposition très simple permet de glisser au fond de cette boîte un châssis de plomb contenant une plaque photographique, sans que les substances cessent d’être enfermées.

Dans ces conditions, à l’abri de toute radiation connue, autre que le rayonnement des parois de la boîte, les substances ont continué à émettre des radiations actives, traversant le verre et le papier noir, et cela depuis plus de six mois pour les unes, et huit mois pour les autres. La dernière épreuve, développée le 7 novembre, est presque aussi intense que les épreuves développées à diverses dates intermédiaires ; si l’on tient compte des différences dans la durée de l’exposition des plaques et dans l’intensité du développement, on constate un affaiblissement très petit entre l’épreuve du 3 mai et l’épreuve du 7 novembre. On voit que la durée de l’émission de ces rayons uraniques est tout à fait en dehors des phénomènes ordinaires de phosphorescence, et l’on n’a pu reconnaître encore où l’uranium emprunte l’énergie qu’il émet avec une si longue persistance.


## Dissipation de la charge des corps électrisés.

On sait que, entre autres propriétés des rayons X, M. J.-J. Thomson a trouvé que non seulement l’action directe de ces rayons décharge à distance un corps électrisé, mais que, après avoir fait agir ces rayons sur une masse de gaz, il suffit de faire passer le gaz sur le corps électrisé pour le décharger. Récemment, M. Villari a montré que les étincelles électriques,mais non l’effluve, communiquaient à divers gaz la même propriété.

Je me suis proposé de rechercher si les rayons uraniques, qui déchargent à distance les corps électrisés, ne communiqueraient pas à divers gaz
cette propriété conductrice.

L’expérience a mis en évidence cette action.

Après divers essais, je me suis arrêté aux dispositions suivantes :

Un courant de gaz (air ou acide carbonique) traverse un tube contenant un tampon de coton pour arrêter les poussières, et un second tube en verre, où l’on pouvait enfermer un sel d’uranium. Ce tube débouche près de la boule d’un électroscope, dont le gâteau de diélectrine a été protégé par un manchon de cuivre.

Dans une autre série d’expériences, on a substitué au dernier tube une boîte en carton, au milieu de laquelle on pouvait disposer un disque d’uranium métallique ; deux ouvertures, dont l’une débouchait près de la boule de l’électroscope, permettaient de faire traverser la boîte par le courant gazeux et de diriger celui-ci sur la boule électrisée.

Dans ces conditions, si l’on ne met pas tout d’abord l’uranium dans l’appareil, l’électroscope reste chargé et ne présente qu’une très faible déperdition, qu’on peut du reste mesurer. Cette déperdition augmente à peine lorsqu’on vient à faire passer le courant gazeux, si celui-ci est bien dépouillé de poussières. Lorsqu’on arrête le courant gazeux et qu’on place l’uranium dans la boîte, ou un sel d’uranium dans le tube, l’électroscope accuse une déperdition due à l’action directe des rayons uraniques. Par exemple, dans une expérience avec l’uranium métallique, la vitesse de la chute des feuilles d’or (exprimée en secondes d’angle par seconde de temps), qui était de 3 sans l’uranium, est devenue 16,7. On a mis alors en mouvement le courant d’air qui, après avoir passé sur l’uranium métallique, a été dirigé sur la boule de l’électroscope ; la dissipation de la charge électrique est devenue considérable ; la vitesse de la chute des feuilles d’or a été 88,6.

L’action de l’air modifié par les rayons uraniques, pour la vitesse particulière du courant employé dans cette expérience, était donc

$$
88,6 - 16,7 = 71,9.
$$

Le courant d’air était obtenu en comprimant, par des poids, de l’air dans un sac en caoutchouc.

Le sulfate double d’uranyle et de potasse, pour des courants d’air à peu près les mêmes, a donné, dans diverses séries, des vitesses de chute des feuilles d’or mesurées par 22,2, 23,0 et 26,5. La moyenne 23,9 peut être comparée à l’action de l’uranium métallique 71,9. Le rapport est 3. Or, les actions directes des rayons uraniques émis par ces deux corps, sur l’électroscope dans l’air, avaient donné antérieurement le rapport 3,65.

Si l’on a égard à ce fait que, dans l’expérience ci-dessus, les fuites de la boîte de carton contenant l’uranium métallique ne dirigeaient pas tout le courant gazeux sur l’électroscope, comme cela avait lieu pour le tube contenant le sel, on reconnaît que le rapport des actions de l’air modifié, dans les deux cas, est sensiblement le même que le rapport des actions directes du métal et du sel sur l’électroscope.

Cette proportionnalité montre que l’effet n’est pas dû à un entraînement de poussières ou de vapeurs du métal ou du sel ; pour en avoir une autre preuve, j’ai enveloppé le disque métallique avec une feuille simple de papier noir, et j’ai recommencé l’expérience. La vitesse de chute des feuilles d’or a été trouvée, après corrections, égale à 12, c’est-à-dire 0,16 de ce qu’elle était avec l’uranium non enveloppé. Des expériences faites, il y a quelques mois, sur l’affaiblissement dû au papier noir quand on faisait agir directement les corps dans l’air au repos, avaient donné 0,115 pour l’uranium, et 0,189 pour le sel.

Les expériences faites avec un courant d’acide carbonique ont donné des résultats du même ordre, mais les courants gazeux étaient très faibles, et la difficulté de régler leur vitesse n’a pas permis d’avoir des nombres aussi directement comparables que les précédents.

En résumé, les observations qui viennent d’être rapportées mettent hors de doute le fait de la décharge des corps électrisés par les gaz ayant été soumis à l’influence des rayons uraniques, et cette propriété, dont le mécanisme reste encore inexpliqué, vient ajouter une relation de plus entre les rayons X et les rayons uraniques, qui, au point de vue de la réflexion et de la réfraction, paraissent être des phénomènes tout à fait différents.
