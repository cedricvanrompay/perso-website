---
title: Sur quelques propriétés nouvelles des radiations invisibles émises par divers corps phosphorescents
lang: fr
date: 1896-03-09
params:
    author: Henri Becquerel
    original_pub_info: Comptes rendus hebdomadaires des séances de l'Académie des sciences, séance du 9 mars 1896
    has_katex: true
---

[voir sur gallica.bnf.fr](https://gallica.bnf.fr/ark:/12148/bpt6k30780/f561.item)


## Action sur les corps électrisés

Dans la dernière séance, j'ai communiqué à l'Académie les observations que j'avais été conduit à faire avec divers sels d'uranium et en particulier avec le sulfate double d'uranyle et de potassium; j'ai montré que ce corps émettait des radiations traversant divers corps opaques pour la lumière, tels que le papier noir, l'aluminium et le cuivre, et que cette émission se produisait, soit sous l'influence de rayons excitant la phosphorescence, soit à l'obscurité, très longtemps après que la substance a cessé d'être excitée par la lumière. Je rappelle également que ces corps n'émettent plus de radiations lumineuses perceptibles, $\frac{1}{100}$ de seconde environ après le moment où ils ont cessé d'être excités par la lumière.

J'ai observé récemment que les radiations invisibles émises dans ces conditions ont la propriété de décharger les corps électrisés soumis à leur rayonnement.

L'expérience se fait très simplement en substituant une lamelle de sulfate double uranico-potassique au tube de Crookes employé dans l'expérience de MM. Benoist et Hurmuzescu.

On sait que l'électroscope de M. Hurmuzescu, protégé contre les influences électriques extérieures par une enveloppe métallique, et contre les radiations ultraviolettes par des verres jaunes, reste chargé pendant de longs mois. Si l'on remplace un des verres jaunes de la lanterne par une lame d'aluminium de 0,12 mm d'épaisseur, et que l'on applique contre cette feuille à l'extérieur une lamelle de la substance phosphorescente, on voit les feuilles d'or de l'électroscope se rapprocher peu à peu, indiquant une décharge lente de l'appareil. En mesurant à des instants suffisament rapprochés l'écartement des feuilles d'or, on a pu dresser des courbes des déviations en fonction du temps, et déterminer, soit la vitesser de déperdition de l'électricité à chaque instant, soit la durée de la décharge de l'appareil chargé chaque fois au même potentiel initial.

Sans donner ici tous les nombres obtenus, j'indiquerai seulement que, dans les conditions qui viennent d'être décrites, une charge qui faisait diverger les feuilles d'or de 18° environ a été dissipée en deux heures cinquante-six minutes.

On a des résultats plus rapides en plaçant les substances rayonnantes directement au dessous des feuilles d'or, dans l'intérieur de la lanterne. Une lamelle de sulfate double uranico-potassique a été ainsi disposée au-dessous des feuilles d'or; cette lamelle mesurait 45 mm de long sur 25 mm de large, et était maintenue à l'abri de la lumière depuis cinq jours.

Les distances ont varié entre 1 cm et 3 cm environ. Les feuilleds d'or divergeant de 12°, la durée de la décharge a été comprise entre vingt et une et vingt-cinq minutes pour une décharge négative, et a été de vingt-trois minutes pour une charge positive produisant la même divergence des feuilles d'or. Cette même charge eût été dissipée en une heure quarante-huit minutes par la même lame placée à l'extérieur derrière la feuille d'aluminium.

Lorsque les corps étaient disposés au-dessous des feuilles d'or, on a interposé une plaque d'aluminium de 2 mm d'épaisseur; la décharge a été alors beaucoup plus lente, et, à partir du moment où les feuilles d'or divergeaient de 12° jusqu'au moment où elles ont cessé de diverger, il s'est écoulé une heure cinquante-deux minutes.

Je me propose d'étudier de plus près les diverses particularités de ce phénomène.


## Réflexion et réfraction

J'ai pu mettre en évidence la réflexion de ces radiations invisibles par les expériences suivantes:

Une lamelle de sulfate uranico-potassique ayant été déposée sur la gélatine d'une plaque Lumière, j'avais recouvert une moitié de cette lamelle par un miroir d'acier dont la face polie était tournée vers la lamelle et la plaque photographique. La plaque développée au bout de cinquante-cinq heures a donné une image très forte; sur la partie non recouverte, les bords de la lamelle étaient assez nets, tandis que les bords de la partie recouverte avaient donné une silhouette beaucoup plus diffuse, comme si une seconde lame, image de la partie couverte, plus éloignée de la gélatine, avait superposé son action à la première.

J'ai disposé alors l'expérience suivante : Dans un petit bloc d'étain, j'ai creusé un miroir hémisphérique dont le poli, bien qu'imparfait, donnait des images. J'ai assujetti, dans le plan focal, une lamelle cristalline dont l'extrémité, de forme triangulaire, occupait un secteur de la base de la calotte sphérique polie, et j'ai placé le tout sur une plaque Lumière, le miroir ayant sa concavité tournée vers la plaque, et la lamelle cristalline étant séparée de la plaque par une cale en papier.

Au bout de quarante-six heures, j'ai développé la plaque, et la silhouette de la lamelle est apparue, la partie triangulaire étant entourée d'un cercle obscur dans lequel on reconnaît la trace d'un défaut du miroir qui, en ce point, n'avait pas pu recevoir de poli.

Cette auréole, à contour assez net, est donc due à des radiations qui, après avoir été réfléchies sur le miroir, ont été renvoyées sur la plaque dans des directions à peu près parallèles.

Les expériences que j'ai instituées en vue de mettre en évidence la réfraction de ces radiations au travers d'un prisme ont donné des indications de réfraction, mais ces indications sont trop faible pour être présentées aujourd'hui. On verra, du reste, par les résultats que je vais décrire plus loin, que certaines images mettent nettement en évidence le fait de la réfraction et de la réflexion totale dans le verre.


## Actions produites par diverses substances et durée de l'émission à l'obscurité

Dans une première série d'expériences, j'ai disposé sur une même plaque photographique divers composés de sesquioxyde d'uranium, des sulfates doubles d'uranyle et de potassium, de sodium, d'ammonium, formant des croûtes cristallines minces, puis un cristal de nitrate d'urane et un morceau de blende hexagonale très phosphorescente, préparée autrefois par M. Sainte-Clare Deville. La plaque photographique était enveloppée d'un papier noir, et les substances fixées chacune sur une lamelle de verre de 0,2 mm d'épaisseur. Pour le nitrate, qui doit être soustrait à l'action de l'humidité de l'air, le cristal a été disposé sur une lamelle identique aux précédents, puis coiffé d'une petite cloche de verre, formée d'un bout de tube, et qui a été scellée à la plaque de verre inférieure de la paraffine. Cette disposition a servi également pour enfermer, à l'abri de l'air, diverses autres substances dont je parlerai plus loin. Ces substances ont été déposées sur la plaque photographique le 3 mars à 4h du soir ; elles étaient depuis longtemps à la lumière diffuse et, depuis lors, elles sont été maintenues constamment à l'obscurité. La plaque, développée le 5 mars à 4h30m, au bout de quarante-huit heures, a montré des actions à peu près équivalentes pour les divers sels d'urane étudiés ; la blende hexagonale n'a manifesté aucune action.

Les mêmes substances, après avoir été retirées de la première plaque, ont été disposées le même jour, à 5h25m, dans le même ordre, sur une seconde plaque appartenant à la même douzaine que la première, enveloppée du même papier noir et dans les mêmes conditions. L'opération a été faite à la lumière d'une bougie éloignée, et à l'abri de la lumière du jour. La plaque, développée le 7 mars à 2h30m, c'est-à-dire après une nouvelle pose de quarante-cinq heures, a donné des résultats aussi nets et des images aussi intenses que les premières. Enfin, une troisième épreuve, semblable aux précédentes, mise en expérience le 7 mars à 3h30m et développée le 9 à 9h5m du matin, a donnée, après quarante-deux heures et demie de pose, des résultats encore presque aussi intenses.

Il est très remarquable de constater que, depuis le 3 mars, c'est-à-dire au bout de plus de cent soixante heures, l'intensité des radiations émises à l'obscurité n'a pas diminué d'une manière sensible. Peut-être faut-il rapprocher ce fait de la conservation indéfinie dans certains corps de l'énergie qu'ils ont absorbée et qu'ils émettent lorsqu'on les échauffe, fait sur lequel j'ai déjà appelé l'attention dans un travail sur la phosphorescence par la chaleur.

Dans les épreuves qui précèdent, les lamelles cristallines donnent des images nettes de leur silhouette parce que ces lamelles sont très minces. Au contraire, le cristal de nitrate d'urane a donné, autour de sa silhouette de la base par laquelle il repose sur le verre mince, une plage légèrement obscure qui est limitée au contour du tube de verre. Cette plage est due à l'action des radations émises obliquement par les faces verticales du cristal qui a plusieurs millimètres d'épaisseur; les radiations arrêtées par ce tube ont été réfractées et réfléchies totalement à l'intérieur, comme le sont les rayons lumineux à l'intérieur d'une veine liquide. L'action est la plus forte dans les régions qui sont au contact du cristal de nitrate d'urane.

Dans une autre série d'expériences, j'ai disposé, à l'extérieur, d'un châssis fermé par une plaque d'aluminium de 2mm d'épaisseur et renfermant une plaque photographique, divers sulfures phosphorescents, du sulfure de calcium, au bismuth, lumineurx bleu, du sulfure de calcium lumineux bleu verdâtre, et un autre échantillon lumineux orangé; puis du du sulfure de strontium très lumineux, vert par phosphorescences, et de la blende hexagonale préparée par M. Ch. Henry.

Ces corps pulvérulents ont été enfermés dans de petits tubes, formant cloche, fermés à la lampe à une extrémité et reposant, par leur partie ouverte, sur une mince lame de verre de 0,2 mm sur laquelle ils étaient scellés avec de la paraffine. Ces plaques de verre ont été disposées côte à côte sur la plaque d'aluminium et le tout, après avoir été exposé à la lumière diffuse, a été enfermé à l'obscurité dans une boîte.

La plaque photographique a été développée après quarante-trois heures de pose. La blende hexagonale n'a encore rien donné, non plus que le sulfure de calcium orangé, ni le sulfure de strontium, mais les deux sulfurs de de calcium lumineux bleu et bleu verdâtre ont donné des actions très énergétiques, les plus intenses que j'aie encore obtenues dans ces expériences. Le fait relatif au sulfure de calcium bleu est d'accord avec l'observation de M. Niewenglowski au travers du papier noir.

Les images que j'ai obtenues avec les deux sulfures de calcium au travers de l'aluminium méritent d'être signalées comme offrant des particularités très importantes. La quantité de poudre phosphorescente contenue dans les tubes formait une colonne dont la hauteur s'élevait de plusieurs millimètres au-dessus de la base plane de la lame de verre, et de près d'un centimètre pour le sulfure bleu. Le rayonnement de la surface latérale a donné de larges taches noires, excessivement fortes, au milieu desquelles on pouvait distinguer en clair l'image de la section du tube de verre, et surtout les bords très nets des lamelles de verra. Ces bords, noirs à l'intérieur, puis entourés, puis entourés d'une ligne absolument blanche, montrent que les radiations obliques ont pénétré dans la lame de verre, y ont été réfractées et réfléchies totalement à la surface de séparation du verre et de l'air. Les deux tubes de sulfure de calcium ont présenté, à des degrés différents, les mêmes apparences, et même les radiations ont été atteindre le tube voisin contenant du sulfure de strontium et ont fait apparaître, avec les mêmes caractères, une partie de ce tube et la lamelle qui le supportait. Si le phénomène de réfraction et de réflexion n'eût pas été mis en évidence par d'autres expériences, il eût été rendu manifeste par cette seule épreuve.

Cette épreuve montre, en outre, que la paraffine a été traversée presque sans absorption par les radiations actives, qui ont ensuite traversé l'aluminium.

Enfin j'ai cherché si l'air n'absorbait pas d'une manière très notable ces radiations sous une petite épaisseur. Des lamelles de sulfate double d'uranyle et de potassium ont été dispoées sur une plaque Lumière à des distances de la gélatine de 0,0 mm, 0,2 mm, 1 mm et 3 mm. Puis on a disposé une seconde plaque, toute semblable. L'une a été laissée en expérience dans l'air, à l'obscurité, et l'autre, également enfermée dans une boîte, a été maintenue pendant le même temps sous la cloche d'une machine pneumatique où l'air était raréfié à quelques centimètres de pression. Les deux plaques ont ensuite été développées au bout de vingt-trois heures de pose; il n'y a pas eu de différence bien sensible entre les épreuves dans l'air ou sous la machine pneumatique; les lamelles n'ont pas donné d'effets très différents depuis la contact jusqu'à 1 mm de distance; mais la silhouette de la lamelle, distante de 3 mm de la gélatine, a été beaucoup plus faible que les autres dans les deux épreuves.