---
title: Émission de radiations nouvelles par l’uranium métallique
lang: fr
date: 1896-05-18
params:
    author: Henri Becquerel
    original_pub_info: Comptes rendus hebdomadaires des séances de l'Académie des sciences, séance du 18 mai 1896
    has_katex: true
---

[voir sur gallica.bnf.fr](https://gallica.bnf.fr/ark:/12148/bpt6k30780/f1088.item)

J’ai montré, il y a quelques mois, que les sels d’uranium émettaient des radiations dont l’existence n’avait pas encore été reconnue, et que ces radiations jouissaient de propriétés remarquables, dont quelques-unes sont comparables aux propriétés du rayonnement étudié par M. Rontgen. Les radiations des sels d’uranium sont émises, non seulement lorsque les substances sont exposées à la lumière, mais encore lorsqu’elles sont maintenues à l’obscurité, et, depuis plus de deux mois, les mêmes fragments de sels divers, maintenus à l’abri de toute radiation excitatrice connue, ont continué à émettre, presque sans affaiblissement sensible, les nouvelles radiations. Du 3 mars au 3 mai, ces substances avaient été renfermées dans une boîte de carton opaque. Depuis le 3 mai, elles sont placées dans une double boîte en plomb, qui ne quitte pas la chambre noire. Une disposition très simple permet de glisser une plaque photographique au-dessous d’un papier noir tendu parallèlement au fond de la boîte, et sur lequel reposent les substances en expérience, sans que celles-ci soient exposées à aucun rayonnement ne traversant pas le plomb.

Dans ces conditions les substances étudiées continuent à émettre des radiations actives.

Si l’on vient à exposer au Soleil, ou mieux à l’arc électrique ou à l’étincelle de la décharge d’une bouteille de Leyde, un fragment d’un des sels maintenu à l’obscurité, on lui communique une légère excitation de l’émission des radiations que nous étudions, mais cette excitation tombe en quelques heures, et la substance reprend son état très lentement décroissant.

J’ai montré également que ces radiations se réfléchissent et se réfractent comme la lumière ; elles décomposent les sels d’argent d’une plaque photographique et l’iodure d’argent déposé sur une lame daguerrienne.

Elles déchargent les corps électrisés et traversent des corps opaques à la lumière tels que le carton, l’aluminium, le cuivre et le platine. L’affaiblissement de ces radiations au travers des écrans que nous venons de citer est moindre que l’affaiblissement du rayonnement émané de la paroi anticathodique d’un tube de Crookes, au travers des mêmes écrans.

Tous les sels d’uranium que j’ai étudiés, qu’ils soient phosphorescents ou non par la lumière, cristallisés, fondus ou dissous, m’ont donné des résultats comparables ; j’ai donc été conduit à penser que l’effet était du à la présence de l’élément uranium dans ces sels, et que le métal donnerait des effets plus intenses que ses composés.

L’expérience faite il y a plusieurs semaines, avec de la poudre d’uranium du commerce, qui se trouvait depuis longtemps dans mon laboratoire, a confirmé cette prévision ; l’effet photographique est notablement plus fort que l’impression produite par un des sels d’uranium et, en particulier, par le sulfate uranico-potassique.

Avant de publier ce résultat, j’ai tenu à attendre que notre Confrère M. Moissan, dont les belles recherches sur l’uranium sont publiées aujourd’hui même, eût pu mettre à ma disposition quelques-uns des produits qu’il avait préparés. Les résultats ont été encore plus nets, et les impressions obtenues sur une plaque photographique au travers du papier noir, avec de l’uranium cristallisé, de l’uranium fondu et du carbure, ont été beaucoup plus intenses qu’avec le sulfate double mis comme témoin sur la même plaque.

La même différence se retrouve dans le phénomène de la décharge des corps électrisés. L’uranium métallique provoque la dissipation de la charge avec une vitesse plus grande que ne le font ses sels. Les nombres suivants, relatifs à l’action d’un disque d’une fonte d’uranium, que m’a obligeamment prêté M. Moissan, donnent une idée de l’ordre de grandeur de cette augmentation.

Dans une première série de mesures, le disque de fonte d’uranium a été placé au-dessous des feuilles d’or d’un électroscope de M. Hurmuzescu et, très près de celles-ci. Pour des charges initiales correspondant à 20° d’écart des feuilles d’or, la vitesse de rapprochement de celles-ci, exprimée en secondes d’angle en une seconde de temps, a été en moyenne 486. On a ensuite recouvert un disque de carton, dont la surface était très sensiblement égale à celle du disque d’uranium, en y disposant des morceaux plats de sulfate double uranico-potassique, et ce disque a été substitué au disque d’uranium. Dans ces conditions la décharge ne se fait pas régulièrement ; la courbe des écarts des feuilles, en fonction du temps, n’est plus une droite, et la vitesse moyenne de dissipation de charges égales aux précédentes a varié de 106,2 à 137,1, suivant la disposition et la forme des lamelles. Le rapport des vitesses correspondant à l’uranium et au sulfate double a donc varié entre 4,56 et 3,54.

Une disposition meilleure consiste à placer les substances en dehors de l’électroscope, au-dessus de la boule de cuivre de la tige, en substituant au chapeau de l’appareil un cylindre métallique fermé par une lame plate percée d’une ouverture convenable. On a obtenu ainsi des décharges très sensiblement proportionnelles aux temps, et les vitesses de déperdition pour des charges écartant les feuilles d’or de 10° ont été 78,75 pour L’uranium et 21,53 pour le sulfate double uranico-potassique. Le rapport de ces deux nombres est 3,65.

Tout en continuant l’étude de ces phénomènes nouveaux, j’ai pensé qu’il n’était pas sans intérêt de signaler l’émission produite par l’uranium, qui, je crois, est le premier exemple d’un métal présentant un phénomène de l’ordre d’une phosphorescence invisible.