---
title: Recherches sur les rayons uraniques
lang: fr
date: 1897-03-01
params:
    author: Henri Becquerel
    original_pub_info: Comptes rendus hebdomadaires des séances de l'Académie des sciences, séance du 1er mars 1897
    has_katex: true
---

[voir sur gallica.bnf.fr]()

## Décharge des corps électrisés.

Parmi les faits que j’ai signalés l’année dernière, comme révélant l’existence des rayons émis par l’uranium, la décharge des corps électrisés est un des phénomènes les plus intéressants. Pour les expériences que je rapporte aujourd’hui, j’ai fait usage d’un électroscope à deux feuilles d’or, isolé par la diélectrine de M. Hurmuzescu, d’électroscopes divers à décharges, et d’un électromètre à piles sèches et à une feuille d’or. En examinant la feuille d’or de ces appareils avec un microscope, dont l’oculaire porte un micromètre, on peut réaliser une grande sensibilité, et mesurer des différences de potentiel d’une fraction de volt. L’électromètre à piles sèches était gradué, à chaque expérience, par comparaison avec les divers éléments d’une pile zinc-cuivre-eau. En prenant la précaution d’envelopper toutes les substances isolantes de cages métalliques reliées à la terre, les expériences se font avec une grande régularité.

Les divers échantillons d’uranium, qui m’ont servi dans ces expériences, m’ont obligeamment été remis par notre Confrère M. Moissan. Le premier est un disque de fonte d’uranium contenant quelques centièmes de carbone, et mesurant 67 mm de diamètre sur 5 mm environ d’épaisseur. Le second est un lingot d’uranium dans lequel M. Werlein m’a habilement taillé une sphère de 13,70 mm de diamètre. Les autres fragments du lingot ont servi à diverses expériences.


### Phénomènes d ’influence

L’uranium décharge à distance, dans l’air, les corps électrisés à des potentiels quelconques ; l’expérience a vérifié le fait depuis moins de 1 volt jusqu’à plus de 3000 volts. La durée de la décharge ne paraît pas différente pour l’électricité positive et l’électricité négative.

Si, après avoir isolé un morceau d’uranium, on l’électrise, il se décharge spontanément par l’air, et la vitesse de la décharge dépend du potentiel. Nous reviendrons plus loin sur ce point.

Lorsqu’on déplace une masse d’uranium métallique, par rapport à d’autres masses conductrices, la capacité électrique du système varie, et comme pour un débit donné d’électricité la vitesse de la chute de potentiel est en raison inverse de la capacité, les résultats des diverses expériences ne sont comparables que si l’on tient compte de cette capacité, ou si on la maintient constante.

Parmi les nombreuses séries d’expériences relatives à la décharge de l’électroscope à deux feuilles d’or par le disque d’uranium, je citerai les suivantes : le bouton de cuivre de l’électroscope, qui avait 15,9 mm de diamètre, était entouré d’un gros tube de paraffine d’un diamètre un peu inférieur au diamètre du disque d’uranium, et sur la base duquel ce disque reposait horizontalement à une distance de quelques centimètres du bouton de l’électroscope. Si l’on charge l’électroscope, on voit ensuite les feuilles d’or se rapprocher peu à peu, et la vitesse de chute varie légèrement suivant que le disque d’uranium est isolé ou relié à la terre. Pour donner une idée de cette variation, je rapporterai seulement que, dans une expérience, le temps que les feuilles d’or ont mis pour se rapprocher de l’écartement de 20° à 10°, ce qui correspondait approximativement à des potentiels de 2500 et de 1400 volts, a été de 163 secondes, quand le disque était isolé, et de 151 secondes lorsqu’il était en communication avec le sol. Cette différence est due à ce que l’uranium, lorsqu’il est isolé, se charge spontanément d’électricité de même signe que la charge qui l’influence.

On peut mettre le fait en évidence par diverses expériences. Par exemple, en répétant l’expérience décrite ci-dessus, on mettra le disque d’uranium isolé en relation avec un électromètre sensible, puis, tout en maintenant le disque au potentiel zéro, on chargera l’électroscope à deux feuilles d’or, d’électricité positive, et l’on supprimera la communication du disque avec le sol. La charge négative, que le disque a prise par influence, se dissipe peu à peu, et l ’électromètre accuse un potentiel croissant, qui atteint bientôt un maximum. Ce maximum resterait fixe, si la charge qui influence le disque restait constante, mais comme celle-ci se dissipe peu à peu par l’effet de l’uranium, le maximum baisse progressivement. En mesurant à la fois les potentiels du disque d’uranium et de l’électroscope, que celui-ci déchargeait, on a obtenu les nombres suivants à divers instants* de la décharge :

<table>
    <caption>Potentiels en volt.</caption>
    <tbody>
        <tr>
            <th scope="row">Électroscope</th>
            <td>1910</td>
            <td>1720</td>
            <td>1475</td>
            <td>1235</td>
            <td>910</td>
            <td>660</td>
            <td>350</td>
            <td>180</td>
        </tr>
        <tr>
            <th scope="row">Disque d'uranium</th>
            <td>18,6</td>
            <td>17</td>
            <td>14,75</td>
            <td>13,3</td>
            <td>12</td>
            <td>11</td>
            <td>10</td>
            <td>9</td>
        </tr>
        <tr>
            <th scope="row">Rapports</th>
            <td>102,7</td>
            <td>101,1</td>
            <td>100</td>
            <td>92,8</td>
            <td>75,8</td>
            <td>60</td>
            <td>35</td>
            <td>20</td>
        </tr>
    </tbody>
</table>

Au lieu d’employer un électromètre, on peut relier le disque d’uranium à un électroscope à décharges très sensible. La feuille d’or indique alors un débit continu, qui s’arrête lorsque le potentiel est devenu inférieur à celui qui est nécessaire pour provoquer la décharge.

Je citerai encore l’expérience suivante : deux petites boules de cuivre isolées ont été disposées à une petite distance l’une de l’autre ; dans l’air, l’une mise en relation avec l’électroscope à décharges ou avec un électromètre, l’autre chargée d’électricité. Dans les conditions ordinaires, l’influence produit un mouvement de la feuille d’or ou une décharge et le phénomène s’arrête là. Si l’on vient alors à approcher des deux boules le disque d’uranium, en communication avec le sol, on voit l’électroscope accuser un débit continu, comme si l’électricité de la boule électrisée passait sur l’autre au travers de l’air. On peut varier l’expérience en mettant la seconde boule tout d’abord au potentiel zéro, puis, lorsqu’on vient à l’isoler, on la voit se charger progressivement et atteindre le potentiel qu’elle aurait pris directement par influence si elle était restée isolée.

Ces expériences réussissent très bien avec des potentiels de quelques volts. Si l’on emploie de forts potentiels, il importe de se mettre à l’abri de la déperdition par convection dans l’air, qui pourrait masquer le phénomène. On pourrait essayer de mettre en évidence le débit d’électricité au moyen d’un galvanomètre, et ce serait la méthode la plus facile pour mesurer exactement ce débit ; mais les quantités d’électricité mises en jeu dans les conditions des expériences présentes ont été trop faibles pour être décelées par ce moyen.

Le disque d’uranium, qui a été employé dans ces expériences, perd très vite les charges d’électricité qu’on lui donne ; l’effet est dû au rayonnement particulier à l’uranium, mais cet effet peut être notablement troublé par les aspérités du lingot.

Afin d’étudier les phénomènes de charge et de décharge dans des conditions plus régulières, j’ai fait tailler la petite sphère d’uranium dont il a été question plus haut, et j’ai refait avec cette sphère des expériences identiques aux expériences précédentes : décharge des corps électrisés, débit par l’électroscope à décharges, etc. La déperdition étant beaucoup moins rapide pour la sphère que pour le disque, celle-ci se charge par influence à un potentiel bien supérieur à celui que prend le disque. Le phénomène a été étudié en plaçant la sphère d’uranium sur un anneau de cuivre, horizontal, maintenu par un fil de cuivre rigide et bien isolé par un bloc de paraffine. Au-dessus de la petite sphère, on avait disposé une boule de cuivre de 15,50 mm de diamètre, isolée et mobile au moyen d’une vis micrométrique de façon à pouvoir l’amener soit au contact, soit à une distance déterminée de la sphère d’uranium.

La sphère d’uranium était en relation avec la feuille d’or de l’électromètre à piles sèches et la boule de cuivre avec un fil qui pouvait, soit être mis à la terre, soit toucher le pôle positif des divers éléments d’une pile zinc-cuivre-eau, dont le pôle négatif était à la terre.

Dans ces conditions, si l’on met d’abord la sphère d’uranium au potentiel zéro, puis la boule de cuivre au potentiel constant V et qu’on isole ensuite le système de la sphère d’uranium et de l’électromètre, on constate que, au lieu de rester à zéro, ce système prend un potentiel croissant qui atteint bientôt un maximum v, proportionnel à V et variable avec la distance des deux boules. Si l’on charge alors le système à un potentiel supérieur à v, celui-ci baisse et retombe à v, qui correspond à un état d’équilibre stable. On a obtenu les valeurs suivantes :

<table>
    <tr>
        <th scope="row">
            Distance des boules
            (en mm)
        </th>
        <td>0,5</td>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
    </tr>
    <tr>
        <th scope="col">
            Potentiels de la sphère de cuivre inductrice V
            (en volts)
        </th>
        <th scope="col" colspan="6">
            Potentiels de la sphère d'uranium v
            (en volts)
        </th>
    </tr>
    <tr>
        <td>5</td>
        <td>1,6</td>
        <td>1,75</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>10</td>
        <td>3,2</td>
        <td>3,6</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>15</td>
        <td>4,9</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>20</td>
        <td>6,4</td>
        <td>6,5</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>25</td>
        <td>7,8</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>30</td>
        <td></td>
        <td>9,3</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>40</td>
        <td>12,4</td>
        <td>12,2</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>50</td>
        <td>16,2</td>
        <td>16</td>
        <td>15,7</td>
        <td>15,1</td>
        <td>14,6</td>
        <td>13,7</td>
    </tr>
</table>


Si l’on substitue à la sphère d’uranium une sphère de cuivre, isolée, le potentiel, quand la distance des boules est de 1 mm, et V = 50 volts, est v = 3,8 volts, environ le quart du potentiel que prend l’uranium dans les mêmes conditions.


### Rôle de l’air dans la décharge

J’ai montré, il y a quelques mois, que les gaz ayant été soumis à l’action des rayons de l’uranium, conservent la propriété singulière de décharger les corps électrisés, lorsque ces gaz viennent en contact avec ces corps. Il en résulte que lorsque la décharge se fait dans un gaz en repos, la propriété conductrice qu’acquiert le gaz doit jouer un rôle dans le phénomène. Par exemple, lorsque la décharge se fait dans l’air, si l’on vient à enlever l’air à mesure qu’il se modifie sous l’influence de l’uranium, la décharge doit être plus lente. C’est, en effet, ce que montrent les expériences que l’on peut réaliser. Dans une première série d’expériences relative à la décharge de l’électroscope à deux feuilles d’or par le disque d’uranium, j’ai employé les dispositions décrites plus haut ; le tube de paraffine avait été muni de deux tubes de verre horizontaux, permettant de faire passer, sur la boule de l’électroscope, un courant d’air traversant l’espace compris entre le cylindre de paraffine et le disque d’uranium qui le fermait à la partie supérieure.

Le courant d’air était obtenu soit par de l’air enfermé dans un sac et comprimé par des poids, soit par une soufflerie. Le disque d’uranium étant en communication avec la terre, la chute des feuilles d’or s’est faite inégalement vite, suivant que l’air était en repos ou en mouvement. On a eu en particulier, pour la durée de la chute de 20° à 10° d’écartement, les valeurs suivantes :

<table>
    <thead>
        <tr>
            <td></td>
            <th scope="col" colspan="2">
                Même vitesse du courant d’air.
            </th>
            <th scope="col" colspan="3">
                Courant d'air plus fort.
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">Séries</th>
            <td>1.</td>
            <td>2.</td>
            <td>3.</td>
            <td>4.</td>
            <td>
                On interpose 2 feuilles
                minces d'aluminium.
            </td>
        </tr>
        <tr>
            <th scope="row">Air en repos</th>
            <td>140,50 s</td>
            <td>136,50 s</td>
            <td>138,00 s</td>
            <td>146,00 s</td>
            <td>191,00 s</td>
        </tr>
        <tr>
            <th scope="row">Air en mouvement</th>
            <td>171,00</td>
            <td>165,00</td>
            <td>173,00</td>
            <td>193,00</td>
            <td>236,00</td>
        </tr>
        <tr>
            <th scope="row">Rapports</th>
            <td>0,82</td>
            <td>0,82</td>
            <td>0,80</td>
            <td>0,76</td>
            <td>0,80</td>
        </tr>
    </tbody>
</table>


L’expérience faite en interposant, entre le disque d’uranium et l’air en mouvement, deux feuilles minces d’aluminium battu, de $\frac{1}{1200}$ de millimètre d’épaisseur environ, et pour laquelle le ralentissement de la décharge est le même, montre bien que l’air modifié intervient dans la décharge. Dans ces expériences le courant d’air était faible ; l’effet augmente avec la vitesse du courant. Par exemple, en plaçant le disque verticalement, à 0,42 mm de distance du bouton de l’électroscope et en projetant sur le disque, dans une direction faisant environ 30° avec la normale, le courant d’air d’une soufflerie de manière à rejeter loin de l’électroscope l’air qui a séjourné près du disque, on a obtenu le ralentissement suivant :

<table>
    <thead>
        <th></th>
        <th>
            Durée de la chute des feuilles d’or
            de 20° à 10°.
        </th>
        <th>
            Rapports.
        </th>
    </thead>
    <tbody>
        <tr>
            <th scope="row">Air en repos</th>
            <td>76 s</td>
            <td>1,000</td>
        </tr>
        <tr>
            <th scope="row">
                Courant d’air des expériences précédentes
            </th>
            <td>102 s</td>
            <td>0,745</td>
        </tr>
        <tr>
            <th scope="row">
                Courant d'air de la soufflerie
            </th>
            <td>232</td>
            <td>0,327</td>
        </tr>
    </tbody>
</table>

Dans cette expérience l’entraînement de l’air a donc réduit l’action de l’uranium au tiers de sa valeur.

Puisque le gaz ambiant joue un pareil rôle, on est conduit à rechercher si en raréfiant l’air on ne diminuerait pas l’action de l’uranium au point de l’annuler. C’est, en effet, ce que l’expérience tend à montrer.

Un accident arrivé à l’appareil que j’avais construit ne m’a pas encore permis d’opérer dans le vide à peu près complet, non plus que de faire des mesures assez précises sur la décharge au milieu de divers gaz. Je continue en ce moment ces expériences. Dans des essais préliminaires faits en enfermant la sphère d’uranium sous la cloche d’une machine pneumatique, j’ai observé que dans l’acide carbonique la décharge est un peu plus rapide et dans l’hydrogène un peu plus lente que dans l’air.

En raréfiant l’air sous la cloche, la décharge devient très lente. Par exemple, la sphère d’uranium étant portée au potentiel de 15 volts, j’ai observé les vitesses de déperdition suivantes, exprimées en fractions de volt et rapportées à la seconde de temps :

<table>
    <thead>
        <tr>
            <th></th>
            <th>Pressions.</th>
            <th>Sphère d’uranium.</th>
            <th>Pressions.</th>
            <th>Sphère d’uranium et lamelle d’uranium.</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th></th>
            <td>760 mm</td>
            <td>0,1640 volts</td>
            <td>761 mm</td>
            <td>0,2140 volts</td>
        </tr>
        <tr>
            <th></th>
            <td>18 mm</td>
            <td>0,0257 volts</td>
            <td>13 mm</td>
            <td>0,0271 volts</td>
        </tr>
        <tr>
            <th>Rapports</th>
            <td>0,154</td>
            <td>0,156</td>
            <td>0,130</td>
            <td>0,127</td>
        </tr>
    </tbody>
</table>

Les nombres de la dernière ligne du Tableau précédent sont les rapports des vitesses de déperdition, et les rapports des racines carrées des pressions. On voit que, d’après ces expériences, les vitesses de déperdition seraient sensiblement proportionnelles à la racine carrée de la densité du milieu gazeux ambiant ; cependant cette assertion demande à être confirmée par des expériences plus précises que je poursuis en ce moment.

### Recherche de la loi de la chute du potentiel en fonction du temps

Le débit d’électricité, par une surface d’uranium déterminée, est fonction de la valeur du potentiel V. La recherche de cette fonction a présenté de nombreuses difficultés, car les électroscopes, disposés pour les expériences dont il vient d’être question, ont des capacités très notablement différentes pour les diverses positions des feuilles d’or. Avec l’électroscope à deux feuilles, la capacité augmente environ de moitié quand les feuilles d’or divergent de 20. La graduation de cet électroscope, et la mesure des capacités, et surtout la détermination des corrections, ont présenté trop d’incertitudes pour que l’on puisse déduire des conclusions précises des nombreux résultats que j’avais accumulés. On peut atténuer l’effet de la variation de la capacité des appareils en leur adjoignant une capacité un peu grande, et constante, mais la lenteur de la chute du potentiel devient telle que d’autres causes d’erreur peuvent s’introduire.

Je citerai, comme exemples, les séries suivantes relatives à la décharge, par le disque d’uranium, des électroscopes réunis à un condensateur à lame d’air :

<table>
    <caption>Faibles potentiels.</caption>
    <thead>
        <tr>
            <th scope="col">V (volts)</th>
            <th scope="col">$\frac{dV}{dt}$</th>
            <th scope="col">$\frac{1}{V} \frac{dV}{dt}$</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>14,3</td>
            <td>0,1150</td>
            <td>0,0080</td>
        </tr>
        <tr>
            <td>13,0</td>
            <td>0,1110</td>
            <td>0,0085</td>
        </tr>
        <tr>
            <td>11,5</td>
            <td>0,0968</td>
            <td>0,0084</td>
        </tr>
        <tr>
            <td>10,1</td>
            <td>0,0882</td>
            <td>0,0087</td>
        </tr>
        <tr>
            <td>8,6</td>
            <td>0,0750</td>
            <td>0,0087</td>
        </tr>
        <tr>
            <td>7,1</td>
            <td>0,0600</td>
            <td>0,0084</td>
        </tr>
        <tr>
            <td>5,5</td>
            <td>0,0461</td>
            <td>0,0084</td>
        </tr>
        <tr>
            <td>4,0</td>
            <td>0,0309</td>
            <td>0,0077</td>
        </tr>
        <tr>
            <td>2,3</td>
            <td>0,0164</td>
            <td>0,0071</td>
        </tr>
    </tbody>
</table>

<table>
    <caption>Forts potentiels.</caption>
    <thead>
        <tr>
            <th scope="col">V (volts)</th>
            <th scope="col">$\frac{dV}{dt}$</th>
            <th scope="col">$\frac{1}{V} \frac{dV}{dt}$</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1516</td>
            <td>6,34</td>
            <td>0,0041</td>
        </tr>
        <tr>
            <td>1399</td>
            <td>6,22</td>
            <td>0,0044</td>
        </tr>
        <tr>
            <td>1281</td>
            <td>6,12</td>
            <td>0,0047</td>
        </tr>
        <tr>
            <td>1159</td>
            <td>6,04</td>
            <td>0,0052</td>
        </tr>
        <tr>
            <td>1032</td>
            <td>5,92</td>
            <td>0,00</td>
        </tr>
        <tr>
            <td>898</td>
            <td>5,77</td>
            <td>0,0064</td>
        </tr>
        <tr>
            <td>753</td>
            <td>5,68</td>
            <td>0,0075</td>
        </tr>
        <tr>
            <td>602</td>
            <td>5,34</td>
            <td>0,0088</td>
        </tr>
        <tr>
            <td>442</td>
            <td>4,74</td>
            <td>0,0107</td>
        </tr>
    </tbody>
</table>

Les nombres de ces deux Tableaux se rapportent à des capacités un peu différentes, et ne sont pas directement comparables.

La loi de la déperdition, dans le cas des faibles potentiels, paraît se rapprocher de la loi du refroidissement des corps ; toutefois, la variation bien nette des rapports calculés montre que l’on ne peut considérer cette conclusion comme établie par les nombres ci-dessus. Entre 1500 et 2000 volts, la vitesse de déperdition n’a pas sensiblement varié. Le fait peut être attribué à l’imperfection des appareils ; j’espère pouvoir prochainement compléter ces premiers résultats.
