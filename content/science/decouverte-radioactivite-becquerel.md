---
title: La découverte de la radioactivité par Henri Becquerel
publishDate: 2025-02-02
lang: fr
---

Le mercredi 26 février 1896, à Paris, Henri Becquerel mène une expérience sur des cristaux phosphorescents. La phosphorescence est la propriété de certains matériaux à émettre de la lumière pendant un certain temps après avoir reçu de la lumière eux-mêmes.

L'expérience consiste donc à exposer les cristaux à la lumière du soleil, puis à les mettre à côté d'une plaque photographique pour mesurer la quantité de lumière qu'ils produisent par phosphorescence.

Mais ce jour-là, à Paris, il n'y pas de soleil. Becquerel va tout de même déveloper la plaque photographique. Comme le cristal n'a reçu que très peu de lumière, il ne s'attend qu'à avoir une image très faible. Et il est choqué quand il voit au contraire une image très nette du cristal sur la photo après développement.

5 jours plus tard, le 2 mars 1896, Becquerel va présenter ce phénomène surprenant à l'académie des sciences. Il se doute qu'il a découvert quelque chose, mais personne, y comprit lui, ne saisit encore à quel point cette découverte va changer la science et le monde.

Le cristal que Becquerel étudiait ce jour-là contenait de l'uranium, et le phénomène que Becquerel a observé s'appelle aujourd'hui la radioactivité.

La découverte de la radioactivité par Henri Becquerel est un sujet passionant pour la vulgarisation scientifique parce qu'on peut la comprendre sans avoir besoin de mathématiques ou de raisonnements complexes. C'est aussi une des découvertes les plus importantes de l'histoire des sciences.

Becquerel présentait régulièrement les résultats de ses travaux à l'académie des sciences, et on peut trouver les notes de ces présentations dans les _« Comptes rendus hebdomadaires des séances de l'Académie des sciences »_, qui sont tous disponibles sur le site Web de la BNF (Bibliothèque Nationale de France), https://gallica.bnf.fr.

Les documents sur le site Web de la BNF sont uniquement des images assez difficiles à lire. J'ai numérisé[^1] toutes les présentations de Henri Becquerel à l'académie des sciences sur le sujet de la radioactivité [(voir la liste complète des ses présentations à l'académie des sciences à partir de 1896)](https://gallica.bnf.fr/ark:/12148/bpt6k32138/f39.item), et on peut donc revivre cette grande aventure scientifique presque comme si on y était. On peut suivre les hésitations, les erreurs et les succès de Becquerel dans ses recherches. La liste complète des textes est disponible [à cette adresse](/archives-numérisées/académie-des-sciences), mais en voici quelques extraits :

[^1]: en utilisant le texte partiellement numérisé du [projet Wikisource](https://fr.wikisource.org)

> J'insisterai particulièrement sur le fait suivant, qui me paraît tout à fait important et en dehors des phénomènes que l'on pouvait s'attendre à observer : Les mêmes lamelles cristalines, placées en regard de plaques photographiques, dans les mêmes conditions et au travers des mêmes écrans, mais à l'abri de l'excitation des radiations incidentes et maintenues à l'obscurité produisent encore les mêmes impressions photographiques.
>
> [séance du 2 mars 1896](/archives-numérisées/académie-des-sciences/becquerel-1896-03-02-radiations-invisibles-corps-phosphorescents/)

> Il est très remarquable de constater que, depuis le 3 mars, c'est-à-dire au bout de plus de cent soixante heures, l'intensité des radiations émises à l'obscurité n'a pas diminué d'une manière sensible.
>
> [séance du 9 mars 1896](/archives-numérisées/académie-des-sciences/becquerel-1896-03-09-nouvelles-propriétés-radiations-invisibles-corps-phosphorescents/)

Pourquoi est-ce que Becquerel est choqué par cette observation ? Parce que l'uranium semble émettre un rayonnement sans aucun apport d'énergie. Avec les autres cristaux phosphorescents, on comprend facilement d'où vient l'énergie qui permet aux cristaux d'émettre de la lumière : elle vient de la lumière qu'ils ont reçue eux-mêmes. Et, d'ailleurs, ils ne produisent de la lumière que pendant un certain temps, après lequel ils sont « à court d'énergie ». Pour les autres phénomènes connus qui produisent de la lumière, on sait aussi d'où vient l'énergie : la flamme d'un feu, le filament d'une ampoule, ou un arc électrique cessent d'émettre de la lumière rapidement quand on arrête de leur fournir de l'énergie en quantité suffisante.

Becquerel va reproduire l'expérience, mais cette fois-ci en évitant complètement d'exposer le sel d'uranium à la lumière.

> Au fond d'une boîte en carton opaque, j'ai placé une plaque photographique, puis, sur la face sensible, j'ai posé une lamelle du sel d'uranium [...]; cette opération étant exécutée dans la chambre noire, la boîte a été refermée, puis enfermée dans une autre boîte en carton, puis dans un tiroir
>
> [séance du 2 mars 1896](/archives-numérisées/académie-des-sciences/becquerel-1896-03-02-radiations-invisibles-corps-phosphorescents/)

Les résultats sont les mêmes : on voit clairement l'image du cristal sur la plaque photographique, donc l'uranium émet un rayonnement sans avoir besoin de source d'énergie extérieure. Plus tard, Becquerel va observer que non seulement l'uranium continue d'émettre des radiations, mais qu'en plus leur intensité semble ne jamais faiblir :

> depuis plus de deux mois, les mêmes fragments de sels divers, maintenus à l’abri de toute radiation excitatrice connue, ont continué à émettre, presque sans affaiblissement sensible, les nouvelles radiations. Du 3 mars au 3 mai, ces substances avaient été renfermées dans une boîte de carton opaque. Depuis le 3 mai, elles sont placées dans une double boîte en plomb, qui ne quitte pas la chambre noire.
>
> [séance du 18 mai 1896](/archives-numérisées/académie-des-sciences/becquerel-1896-05-18-radiations-nouvelles-uranium-métallique)

Ça n'est pas la seule propriété étrange de ces « sels d'uraniums » que Becquerel a dans son laboratoire. Ces « radiations » qui en émanent sont visibles sur une plaque photographique de l'époque (« _Une plaque Lumière, au gélatino-bromure d'argent_ » d'après Becquerel), mais elles sont invisibles à l'œil nu. Aussi, et surtout, ces radiations traversent certains matériaux qui pourtant ne laissent pas passer la lumière visible :

> On peut vérifier très simplement que les radiations émises par cette substance [...] traversent, non seulement des feuilles de papier noir, mais encore divers métaux, par exemple une plaque d'alumunium et une mince feuille de cuivre.
>
> [séance du 2 mars 1896](/archives-numérisées/académie-des-sciences/becquerel-1896-03-02-radiations-invisibles-corps-phosphorescents/)

Est-ce que ce ne sont pas des propriétés encore plus incroyables ? Pourquoi est-ce que ce qui _« paraît tout à fait important et en dehors des phénomènes que l'on pouvait s'attendre à observer »_ à Becquerel est le fait que l'uranium émette des radiations sans avoir besoin d'une source d'énergie, et non pas le fait que ces radiations sont invisibles et traversent la matière ?

En fait, si ces propriétés sont assez surprenantes en 1896, elles ne sont pas nouvelles. Presque cent ans plus tôt, en 1801, le physicien allemand Johann Wilhelm Ritter avait déjà découvert les rayons ultraviolets, qui sont invisibles à l'œil nu. Là aussi, c'est en utilisant une plaque photographique que Ritter se rend compte de l'existence de ces rayons.

Mais surtout, en 1895, Wilhelm Conrad Röntgen découvre les rayons X, qui non seulement sont invisibles à l'œil nu, mais aussi traversent la matière.

Quand Becquerel découvre le rayonnement émis par l'uranium, la communauté scientifique du monde entier est donc déjà fascinée par les rayons X et leur capacité à traverser la matière. En fait, si on lit le compte-rendu des autres présentations du 2 mars 1896 à l'académie des sciences, par exemple [(voir sur gallica.bnf.fr)](https://gallica.bnf.fr/ark:/12148/bpt6k30780/f499.item), il y a 8 présentations dédiées aux rayons X, sur un total de 35 présentations ce jour-là, avec des sujets aussi variés que la géologie ou la botanique.

C'est pour cela que, de toutes les propriétés du rayonnement de l'uranium, c'est l'absence de source d'énergie extérieure qui choque le plus Becquerel et la communauté scientifique. C'est la seule propriété qui est complètement nouvelle.

Malheureusement pour Becquerel, la fascination générale pour les rayons X veut aussi dire qu'il va être très difficile d'attirer l'attention sur une nouvelle découverte, d'autant plus que le rayonnement naturel de l'uranium est bien moins fort que les rayons X qu'on arrive déjà à produire en 1896 avec suffisamment de puissance pour photographier le squelette d'une personne à travers son corps.

Ce sera Marie Curie qui plus tard arrivera à attirer véritablement l'attention sur ce phénomène. Mais Henri Becquerel a tout de même eu des contributions importantes dans l'étude de la radioactivité, qui permettront ensuite à Marie Curie de connaître le succès.

La principale contribution de Becquerel va sans doute être le fait de développer une technique pour mesurer la quantité de radiation émise par l'uranium d'une manière plus précise que l'utilisation d'une plaque photographique. Becquerel va réutiliser une expérience qui avait été faite avec les rayons X et qui consiste à exposer aux rayons un appareil appelé « électroscope » qui mesure une charge électrique. Les rayons rendent l'air conducteur, faisant se décharger l'électroscope: 

> On sait que l'électroscope de M. Hurmuzescu, protégé contre les influences électriques extérieures par une enveloppe métallique, et contre les radiations ultraviolette par des verres jaunes, reste chargé pendant de longs mois. Si l'on remplace un des verres jaunes de la lanterne par une lame d'aluminium de 0,12 mm d'épaisseur, et que l'on applique contre cette feuille à l'extérieur une lamelle de la substance phosphorescente, on voit les feuilles d'or de l'électroscope se rapprocher peu à peu, indiquant une décharge lente de l'appareil.
>
> [séance du 9 mars 1896](/archives-numérisées/académie-des-sciences/becquerel-1896-03-09-nouvelles-propriétés-radiations-invisibles-corps-phosphorescents/)

C'est la propriété que Becquerel va étudier le plus et c'est celle qui va permettre à Marie Curie de faire ces premières découvertes majeures. Marie Curie va faire des découvertes majeures en utilisant l'électromètre piézoélectrique conçu par son mari Pierre Curie qui est beaucoup plus précis que l'électroscope de Hurmuzescu.

On peut se demander pourquoi Henri Becquerel avait de l'uranium dans son laboratoire. Aujourd'hui quand on parle d'uranium on pense aux centrales nucléaires, à la bombe atomique et à la haute technologie en général. En fait, en 1896, la communauté scientifique connaît l'uranium depuis environ 100 ans déjà, depuis sa découverte par le chimiste prussien Martin Heinrich Klaproth qui la nomme en honneur de la récente découverte de la planète Uranus.

L'uranium était aussi utilisé dans la fabrication du verre longtemps avant d'être identifié par les scientifiques, pour donner une couleur jaune ou vert. On a même trouvé du verre coloré à l'uranium datant de l'antiquité [(voir cet article de Peter Kurzmann)](https://www.arche-kurzmann.de/P%203%20Antique%20Non-Roman%20Glass%20Mosaic%20in%20Italy.htm).

Dans [les Annales de Chimie et de Physique, tome XXXVIII](https://gallica.bnf.fr/ark:/12148/bpt6k34779r/f3.item), il y a une traduction d'un mémoire présenté par George Gabriel Stokes à la société royale de Londres en 1852. Stokes parle de corps qui, quand on les éclaire avec une certaine lumière, produisent une lumière différente. Et à la page 500, il y a ce paragraphe:

> Certains échantillons de verre jaune de Bohême ont été signalés par M. Brewster comme diffusant une belle lumière verte. M. Stokes a trouvé la même proriété dans le verre jaune de fabrication anglaise désigné dans le commerce sous le nom de _canary glass_.

Le verre jaune de Bohême et le _canary glass_ anglais sont des verres qui contiennent de l'uranium.

En 1852, quand Stokes présente ses travaux, Henri Becquerel vient tout juste de naître. Mais il y a un scientifique Français de cette époque qui travaille sur la lumière et qui va s'intéresser aux recherches de Stokes : c'est Edmond Becquerel, le père de Henri. Il est d'ailleurs cité dans le mémoire des Annales de Chimie et de Physique cité plus haut :

> Dans l'expérience où l'on reçoit un spectre pur sur un vase plein d'une dissolution de sulfate de quinine, la lumière bleue diffusée qui s'étend au delà du violet présente de larges bandes obscures [...]. M. Stokes a donné, _fig. I, Pl; I_, un dessin de ces bandes obscures, telles qu'elles se manifestent dans ses expériences, et ce dessin a présenté la plus grande analogie avec celui que M. Edmond Becquerel a tracé, d'après des expériences photographiques.
>
> source: https://gallica.bnf.fr/ark:/12148/bpt6k34779r/f494.item

En 1857 et 1858, Edmond Becquerel va publier deux mémoires compilés sous le titre [_« Recherches sur divers effets lumineux qui résultent de l'action de la lumière sur les corps »_](https://gallica.bnf.fr/ark:/12148/bpt6k34796b/f4.item). Il y parle des travaux de Stokes sur la lumière et mentionne les résultats sur le verre coloré, ici appelé « verre d'urane » :

> Ce physicien a observé ces effets dans des corps qui ne sont pas doués de phosphorescence, et parmi lesquels on peut citer, comme on le sait, le bisulfate de quinine, le verre d'urane, une dissolution alcoolique de chlorophylle, etc.
>
> https://gallica.bnf.fr/ark:/12148/bpt6k34796b/f60.item

Edmond Becquerel dit que ces corps « ne sont pas doués de phosphorescence ». En fait, il y a bien un effet de phosphorescence dans ces corps, mais il dure tellement peu de temps qu'il est imperceptible pour l'œil humain. Edmond Becquerel va comprendre cela et construire un appareil qu'il appelle « phosphoroscope », qui est capable de mesurer des phénomènes de phosphorescence aussi courts.

C'est donc parce que la phosphorescence est difficile à observer sur le verre d'urane que Edmond Becquerel va s'y intéresser en particulier, et quand Henri Becquerel va continuer les travaux de son père sur la phosphorescence, il va utiliser entre autre des matériaux contenant de l'uranium. Henri Becquerel mentionne d'ailleurs les travaux de son père dans ses présentations sur l'uranium à l'académie des sciences:

> Les caractères des radiations lumineuses émises par cette substance ont été étudiés autrefois par mon père et j'ai eu, depuis, l'occasion de signaler quelques particularités intéressantes que présentent ces radiations lumineuses.
>
> [séance du 2 mars 1896](/archives-numérisées/académie-des-sciences/becquerel-1896-03-02-radiations-invisibles-corps-phosphorescents/) 

Au final, la radioactivité de l'uranium que Henri Becquerel va découvrir n'a rien à voir avec la phosphorescence de l'uranium que son père étudiait. C'est une découverte en grande partie due au hasard. Mais ça n'est pas que du hasard. Les scientifiques avaient-ils déjà l'impression qu'il y avait « quelque chose d'étrange » avec l'uranium ? Dans les années 1850-1860, Abel Niépce de Saint-Victor présentait à l'académie des sciences les résultats de ses travaux sur la photographie (voir par exemple [sa présentation du 1er mars 1858](https://gallica.bnf.fr/ark:/12148/bpt6k3003h/f448.item)), où il utilisait fréquemment de l'uranium. Il mentionne les travaux d'Edmond Becquerel, et suggère l'existence d'un « rayonnement invisible à l'œil nu » :

> J'ai ensuite enveloppé une aiguille dans un papier imprégné d'azotate d'urane [...].
>
> Il résulte de l'ensemble de mes expériences que cette activité persistante donnée par la lumière à tous les corps poreux, même les plus inertes, ne peut même pas être de la phosphorescence, car elle ne durerait pas si longtemps, d'après les expériences de M. Edmond Becquerel; il est donc plus probable que c'est un rayonnement invisible à nos yeux
>
> [séance du 1er juillet 1861](https://gallica.bnf.fr/ark:/12148/bpt6k3010v/f34.item)

-----------------

_Merci à Valeria Tettamanti pour sa relecture et ses suggestions, et à [Kathy "Kathy Loves Physics" Joseph](https://kathylovesphysics.com/) pour avoir la fait [la vidéo qui m'a donné envie de me lancer dans ce travail](https://youtu.be/64q1R56hFjY)._
