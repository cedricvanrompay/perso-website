---
title: My Publications
---

* Fast Two-Server Multi-User Searchable Encryption with Strict Access Pattern Leakage. With [Refik Molva](https://www.eurecom.fr/en/people/molva-refik) and [Melek Önen](https://www.eurecom.fr/~onen/). Presented at ICICS 2018. 
* Breaking and Fixing the Security Proof of Garbled Bloom Filters. With [Melek Önen](https://www.eurecom.fr/~onen/). Presented at DBSec 2018. 
* Secure and Scalable Multi-User Searchable Encryption. With [Refik Molva](https://www.eurecom.fr/en/people/molva-refik) and [Melek Önen](https://www.eurecom.fr/~onen/). Presented at SCC@AsiaCCS 2018. [eprint version](https://eprint.iacr.org/2018/90) 
* A Leakage-Abuse Attack Against Multi-User Searchable Encryption.  With [Refik Molva](https://www.eurecom.fr/en/people/molva-refik) and [Melek Önen](https://www.eurecom.fr/~onen/). Presented at the PET Symposium 2017. [eprint version](https://eprint.iacr.org/2017/400) 
* Multi-user Searchable Encryption in the Cloud. With [Refik Molva](https://www.eurecom.fr/en/people/molva-refik) and [Melek Önen](https://www.eurecom.fr/~onen/). Presented at ISC 2015 