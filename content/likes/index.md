---
title: Things I Like
---

[Voir aussi la version française](../aime/)


## I.T.

For content about information technology (info tech or I.T.), see [the dedicated page](./info-tech).


## Video Channels

- [Never Too Small](https://www.youtube.com/@nevertoosmall):
    beautiful and relaxing videos
    about architecture and interior design for small spaces.
- [Daniel Titchener](https://www.youtube.com/@DanielTitchener):
    also architecture for small spaces
- [Steve Mould](https://www.youtube.com/@SteveMould): engineering and physics
- [Journey to the Microcosmos](https://www.youtube.com/@journeytomicro): beautiful videos about the microscopic world
- [Economics Explained](https://www.youtube.com/@EconomicsExplained): economics
- [Kathy Loves Physics & History](https://www.youtube.com/c/KathyLovesPhysicsHistory): history of Physics. In particular, I liked a lot [her playlist on the history of electricity](https://www.youtube.com/playlist?list=PLepnjl2hm9tF-CxhyRFZi3Pujkq_V4pKP)

The following channels are also available on  [Nebula](https://nebula.tv/):
- [Practical Engineering](https://www.youtube.com/@PracticalEngineeringChannel): civil engineering
- [Real Engineering](https://www.youtube.com/@RealEngineering): engineering, mostly high-tech
- [Real Life Lore](https://www.youtube.com/@RealLifeLore): mostly geopolitics
- [Religion for Breakfast](https://www.youtube.com/@ReligionForBreakfast): academic study of religion
- [Medlife Crisis](https://www.youtube.com/@MedlifeCrisis): medicine


## Movies

- [The Peanut Butter Falcon](https://www.imdb.com/title/tt4364194/)
- [The Way Way Back (2013)](https://www.imdb.com/title/tt1727388/)
- [Gifted (2017)](https://www.imdb.com/title/tt4481414/)
- Most films by [Ruben Östlund](https://en.m.wikipedia.org/wiki/Ruben_%C3%96stlund): [The Square (2017)](https://www.imdb.com/title/tt4995790/), [Triangle of Sadness (2022)](https://www.imdb.com/title/tt7322224/), and [Force Majeure (2014)](https://www.imdb.com/title/tt2121382/)
