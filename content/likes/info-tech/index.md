---
title: Info Tech Things I Like
---


## News Channels

* [Hacker News](https://news.ycombinator.com/), of course. On mobile I use a different frontend: https://www.hckrnws.com
* [Frank Denis' Mastodon channel](https://mastodon.social/@jedisct1)


## Video Channels

* [Fireship](https://www.youtube.com/@Fireship): software
* [HTTP 203](https://www.youtube.com/playlist?list=PLNYkxOF6rcIAKIQFsNbV0JDws_G_bnNo9): web technology
* [Asianometry](https://www.youtube.com/@Asianometry): technology, mostly semiconductors
* [DIY Perks](https://www.youtube.com/@DIYPerks): high-tech DIY
* the video channel of some conferences:
    * [GOTO](https://www.youtube.com/@GOTO-)
    * [Strange Loop](https://www.youtube.com/c/StrangeLoopConf/videos)
    * GopherCon (videos can be found on the [Gopher Academy YouTube channel](https://www.youtube.com/@GopherAcademy))
    * [PyCon US](https://www.youtube.com/@PyConUS)


## Videos

* Two videos on UNIX system calls: [part one](https://youtu.be/xHu7qI1gDPA) and [part two](https://youtu.be/2DrjQBL5FMU)


## Books

- [A Mind at Play: How Claude Shannon Invented the Information Age](https://www.goodreads.com/book/show/32919530-a-mind-at-play)