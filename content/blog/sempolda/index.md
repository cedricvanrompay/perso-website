---
title: Sempolda, un site Web pour explorer l'actualité politique en France sur les sujets qui vous intéressent
publishDate: 2024-09-24
lang: fr
---

Je suis fier d'annoncer que mon projet personnel [Sempolda](https://sempolda.fr/) est prêt à être testé par tout le monde. Il reste beaucoup à faire et je vais continuer de l’améliorer, mais il est temps que je cherche aussi des premiers utilisateurs pour obtenir des avis, des demandes, des questions, etc.

Il y a quatre ans, pendant le premier confinement, je me suis dit qu'il faudrait construire un site web pour voir facilement ce que font le gouvernement, les parlementaires et l'ensemble de la classe politique en général, sur n'importe quel sujet qui pourrait m'intéresser.

Cela m'arrive régulièrement, en écoutant la radio ou en lisant le journal, de vouloir plus de détails sur le sujet dont j'entends parler. On peut trouver ces détails sur Internet, sur le site de l'Assemblée nationale par exemple, mais même si l'information est là il n'y a pas de moyen efficace de faire des recherches dans cette masse d'information.

J'ai créé un programme qui va sur le site de l'Assemblée nationale et copie les transcriptions des débats, puis j'ai créé une interface qui me permet de visualiser le contenu qui a été récupéré et noter de quels sujets parle chaque document. Enfin j'ai créé une autre interface, très simple, qui permet de chercher tous les documents liés à un sujet en particulier, et c'est cette interface que vous pouvez voir aujourd'hui à l'adresse https://sempolda.fr.

{{<figure src="screenshot.png" caption="capture d'écran de Sempolda" >}}

Pourquoi est-ce qu'il m'a fallu quatre ans avant d'avoir une version que je me sente prêt à annoncer publiquement ? Déjà, il s'agit d'un projet personnel sur lequel je travaille sur mon temps libre, quand j'en ai envie. Aussi, même si cela fait longtemps que je fais du développement web, il y a eu un certain nombre de choses que j'ai dû apprendre depuis le début pour ce projet. Je me suis aussi laissé la liberté d'expérimenter plusieurs approches et plusieurs technologies différentes à certains moments. Mais surtout, j'ai dû construire un programme qui fasse la majeure partie de l'annotation à ma place.

Au début du projet, je pensais que le travail d'annoter les documents, c'est-à-dire de noter les sujets dont ils parlent, pourrait fonctionner comme dans Wikipédia, avec un certain nombre de contributeurs volontaires. Il y a sans doute des gens qui lisent ces documents de toute façon, que ce soit parce que c'est leur travail ou parce que ça les intéresse, donc si chacun annote une fraction de ce qu'il lit, on peut arriver assez rapidement à annoter une grande partie de ce qu'il y a sur le site de l'assemblée. Mais c'est difficile de trouver ces gens-là, surtout que je connais peu de personnes dont le travail consiste à lire les documents de l'assemblée nationale.

J'ai donc passé plusieurs années à annoter moi-même un certain nombre de documents, tout en construisant un programme qui pourrait faire ce travail à ma place. Cela m'a pris un peu de temps, mais aujourd'hui j'ai un programme qui fait peu d'erreurs, et aucune grosse erreur. Je continue d'annoter moi-même les documents qu'il n'a pas réussi à annoter, et à vérifier et corriger une partie des annotations faites par la machine, mais le résultat est globalement satisfaisant. Et le programme ne va que devenir meilleur avec le temps.

Il est donc temps que je montre Sempolda à un maximum de personnes, pour avoir des avis extérieurs qui m'aident à décider comment l'améliorer et quelles directions lui faire prendre.

Sempolda est gratuit, sans pub, sans aucune source de revenu. Pour l'instant, c'est juste un projet sur lequel je travaille parce que ça me plaît. Si un jour il attire beaucoup d'attention, je pourrais envisager de chercher un moyen d'en faire mon travail principal. Pour ceux que ça intéresse, le code source de Sempolda est à l'adresse https://gitlab.com/cedricvanrompay/sempolda.

Pour l'instant Sempolda ne contient que les questions au gouvernement de l'assemblée nationale, parce que c'était plus facile de commencer avec un seul type de texte à indexer, mais maintenant il va falloir penser à ajouter d'autres ressources et voir par lesquelles commencer : les questions au gouvernement du sénat ? les projets de lois ? des textes venant de l'Union Européenne ? les rapports de commissions ? les rapports d'agences comme l'INSEE, France Stratégie, ou l'ADEME ? des émissions de radio comme [« entendez-vous l'éco »](https://www.radiofrance.fr/franceculture/podcasts/entendez-vous-l-eco) ? Dans l'absolu, il faudrait que l'utilisateur qui clique sur un sujet ait toutes les ressources les plus importantes et les plus récentes sur le sujet en question.

Toutes les questions, commentaires, suggestions, encouragements ou autres sont les bienvenues : cedric.vanrompay@gmail.com.
