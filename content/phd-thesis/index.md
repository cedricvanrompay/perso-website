---
title: My PhD Thesis
---

[Download PDF](cedricvanrompay-phd-thesis.pdf)

The thesis is in English, with a French summary at the beginning.


## Title

Multi-User Searchable Encryption (in French: Protocoles Multi-Utilisateurs de Recherche sur Bases de Données Chiffrées)


## Jury

* M. Bruno MARTIN, Professeur, Laboratoire I3S, Université Nice Sophia Antipolis (rapporteur)
* M. Roberto DI PIETRO, Professeur, Hamad Bin Khalifa University (rapporteur)
* M. Pascal LAFOURCADE, Maître de Conférences HDR, LIMOS, Université Clermont Auverge (examinateur)
* M. David POINTCHEVAL, Directeur de Recherches, DI, ENS (examinateur, président du jury)
* M. Refik MOLVA, Professeur, EURECOM (Directeur de Thèse)
* Mme. Melek ONEN, Maître de Conférence HDR, EURECOM, (Directrice de Thèse)


## Table of Contents

* Remerciements
* Résumé en Français
* 1 Introduction
* 2 Preliminaries
    * 2.1 General Notations
    * 2.2 Hard Problems and security proofs
    * 2.3 Cryptographic Primitives
* 3 Multi-Reader Multi-Writer Searchable Encryption
    * 3.1 Motivations
    * 3.2 Definitions
    * 3.3 State of the Art
    * 3.4 Leakage Abuse Attacks against Searchable Encryption
* 4 The Dilemma of MUSE: On the difficulty of trading privacy for efficiency
    * 4.1 The Case for User-Server Collusions
    * 4.2 Insecurity of Iterative Testing Against User-Server Collusions
    * 4.3 Leakage-Abuse Attacks Applied to MUSE
    * 4.4 Lessons from the MKSE Protocol
    * 4.5 The Dilemma of MUSE
* 5 A First Generation of Secure MUSE protocols: choosing between access pattern leakage and scalability issues
    * 5.1 Record Preparation and the use of Two Non-Colluding Servers
    * 5.2 The DH-AP-MUSE protocol: fast MUSE with Access Pattern Leakage
    * 5.3 The RMÖ15 protocol: Very low leakage at the price of scalability
    * 5.4 Conclusion
* 6 A Secure yet Scalable MUSE protocol
    * 6.1 Result Filtering, Result Length Leakage and Response Unlinkability
    * 6.2 New primitives for response unlinkability
    * 6.3 The RMÖ18 MUSE Protocol
    * 6.4 Further Remarks and Implementation
    * 6.5 Conclusion
