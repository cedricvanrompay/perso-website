import subprocess
import os

from . import config

def build(staging: bool):
    if not os.path.isdir('build'):
        os.mkdir('build')

    cwd = os.getcwd()

    flags = [
        '--destination /public',
    ]

    if staging:
        flags.append('--baseURL https://staging.cedricvanrompay.fr/')

    subprocess.run(' '.join([
        'docker run --rm',
        f'-v {cwd}:/project',
        f'-v {cwd}/build/www:/public',
        f'ghcr.io/gohugoio/hugo:{config.HUGO_VERSION}',
        "build",
        *flags,
    ]).split(), check=True)


    with open('redirections.txt') as source:
        with open('build/redirections', 'w') as dest:
            
            dest.write("# to import in the caddyfile\n\n")

            for line in source:
                line = line.strip()

                if (not line) or line.startswith('#'):
                    continue

                old, new = line.split()

                # see https://caddyserver.com/docs/caddyfile/directives/redir
                redirection = f"redir {old} {new}"

                dest.write(redirection+'\n')
            
