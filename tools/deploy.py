#!/usr/bin/env python3

import subprocess

from .build import build

def deploy(staging: bool):
    build(staging)

    host = (
        'staging.cedricvanrompay.fr' if staging
        else 'cedricvanrompay.fr'
    )

    # TODO redirections

    subprocess.run([
        'rsync',
        '--recursive',
        '--delete',
        '--verbose',
        # note that in shell we need double quotes around "sudo rsync"
        # but here we must NOT put them or it will break
        '--rsync-path=sudo rsync',
        'build/',
        f'ubuntu@{host}:/var/www/{host}/',
    ], check=True)