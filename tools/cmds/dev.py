#!/usr/bin/env python3
import sys
import os
import subprocess
# assuming script is invoked from the repository root
sys.path.append(os.getcwd())

from tools import config

cwd = os.getcwd()

subprocess.run(' '.join([
    'docker run --rm -it',
    f'-v {cwd}:/project',
    f'-p 1313:1313',
    f'ghcr.io/gohugoio/hugo:{config.HUGO_VERSION}',
    'server',
    "--disableFastRender",
    "--bind 0.0.0.0",
    "--destination build/www"
]).split(), check=True)