#!/usr/bin/env python3
import sys
import os

sys.path.append(os.getcwd())

from tools.build import build

build(staging=True)