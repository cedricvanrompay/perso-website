#!/usr/bin/env python3
import argparse
import sys
import os

sys.path.append(os.getcwd())

from tools.deploy import deploy

parser = argparse.ArgumentParser()
parser.add_argument('env', choices=['staging', 'prod'])
args = parser.parse_args()

deploy(staging=(args.env == 'staging'))
